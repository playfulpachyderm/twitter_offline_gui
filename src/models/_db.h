#include <QSqlDatabase>
#include <QString>

#include <exception>

extern QSqlDatabase DB;

class DBException : public std::exception {
 public:
    explicit DBException(QString);
    ~DBException();
    const char* what() const throw();

 protected:
    QString msg;
    const char* c_msg;
};

void set_profile_directory(QString path);
void create_profile_directory(QString path);
