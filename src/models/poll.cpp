#include "poll.h"

#include <QSqlQuery>
#include <QVariant>
#include <QDateTime>

#include "./_db.h"

const QString Poll::SQL_POLL_FIELDS = "id, num_choices, choice1, choice1_votes, choice2, "
    "choice2_votes, choice3, choice3_votes, choice4, choice4_votes, voting_duration, "
    "voting_ends_at, last_scraped_at";


int Poll::total_votes() const {
    int result = 0;
    for (int i: votes) {
        result += i;
    }
    return result;
}

QString Poll::format_votes_count(int votes_count) const {
    double percentage = static_cast<double>(votes_count * 1000 / total_votes()) / 10;
    return QString::number(votes_count) + " (" + QString::number(percentage) + "%)";
}

bool Poll::is_closed() const {
    return voting_ends_at < QDateTime::currentSecsSinceEpoch();
}

int Poll::get_winner() const {
    int ret = -1;
    int winning_score = -1;
    for (uint i = 0; i < votes.size(); i++) {
        if (votes[i] > winning_score) {
            ret = i;
            winning_score = votes[i];
        }
    }
    return ret;
}


QString Poll::render_closes_at() const {
    return Poll::render_closes_at(voting_ends_at, QTimeZone::systemTimeZone());
}

QString Poll::render_closes_at(int voting_ends_at, QTimeZone timezone) {
    return QDateTime::fromSecsSinceEpoch(voting_ends_at)
        .toTimeZone(timezone)
        .toString("MMM d, yyyy h:mm a").remove(".");
}


vector<Poll> Poll::get_polls_for_tweet_id(TweetID id) {
    QSqlQuery query(DB);
    query.prepare("select " + Poll::SQL_POLL_FIELDS + "  from polls where tweet_id = :tweet_id");
    query.bindValue(":tweet_id", (long long) id);
    query.exec();

    vector<Poll> ret;

    while (query.next()) {
        Poll p;
        p.id = query.value(0).toULongLong();
        p.num_choices = query.value(1).toInt();

        p.voting_duration = query.value(10).toInt();
        p.voting_ends_at = query.value(11).toInt();
        p.last_scraped_at = query.value(12).toInt();

        for (int i = 0; i < p.num_choices; i++) {
            p.choices.push_back(query.value(2*i + 2).toString());
            p.votes.push_back(query.value(2*i + 3).toInt());
        }
        ret.push_back(p);
    }
    return ret;
}
