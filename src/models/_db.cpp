#include "_db.h"

#include "../lib/global_state.h"


QSqlDatabase DB;

DBException::DBException(QString msg) {
    this->msg = msg;
    char* tmp = new char[msg.size() + 1];  // extra null character. ffs
    snprintf(tmp, msg.size() + 1, "%s", msg.toStdString().c_str());
    this->c_msg = tmp;
}
const char* DBException::what() const throw() {
    return this->c_msg;
}
DBException::~DBException() {
    delete[] c_msg;
}

/**
 * Load the database driver if it hasn't been loaded yet.  Close any existing connection, then
 * connect to the given DB path.
 */
void set_profile_directory(QString path) {
    if (!DB.isValid()) {
        // No database has been created yet; create one.
        DB = QSqlDatabase::addDatabase("QSQLITE");
    } else {
        // The database is already initialized.  Close the existing connection before opening the new one
        DB.close();
    }
    QString db_path = path + "/twitter.db";
    DB.setDatabaseName(db_path);
    DB.setConnectOptions("QSQLITE_OPEN_READONLY");
    if (!DB.open()) {
        throw DBException("Could not open: " + db_path);
    }

    GlobalState::PROFILE_DIR = path;

    Tweet::init_tombstone_types();
}

/**
 * Make an engine call to create a new profile directory
 */
void create_profile_directory(QString path) {
    // TODO: error checking
    GlobalState::TWITTER_ENGINE.call(
        "twitter",
        QStringList() << "create_profile" << path
    );
}
