#ifndef URL_MODEL_H
#define URL_MODEL_H

#include <QString>

#include <vector>

#include "typedefs.h"

using std::vector;

class Url {
 private:
	QString thumbnail_local_path;

 public:
	TweetID tweet_id;
	QString domain;
	QString text;
	QString title;
	QString description;
	QString get_thumbnail_path() const;
	int thumbnail_width;
	int thumbnail_height;

	UserID creator_id;
	UserID site_id;

	bool has_card;
	bool has_thumbnail;
	bool is_content_downloaded;

	static vector<Url> get_urls_for_tweet_id(TweetID);
	static const QString SQL_URL_FIELDS;
};

#endif  // URL_MODEL_H
