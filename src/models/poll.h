#ifndef POLL_H
#define POLL_H

#include <QString>
#include <QTimeZone>

#include <vector>

#include "typedefs.h"

using std::vector;

class Poll {
 public:
    PollID id;
    int num_choices;

    vector<QString> choices;
    vector<int> votes;

    int voting_duration;  // seconds
    int voting_ends_at;
    int last_scraped_at;

    // Getters
    int total_votes() const;
    QString format_votes_count(int votes) const;
    bool is_closed() const;
    int get_winner() const;
    QString render_closes_at() const;
    static QString render_closes_at(int voting_ends_at, QTimeZone timezone);


    static vector<Poll> get_polls_for_tweet_id(TweetID);
    static const QString SQL_POLL_FIELDS;
};

#endif  // POLL_H
