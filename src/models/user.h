#ifndef USER_H
#define USER_H

#include <QSqlDatabase>
#include <QSqlQuery>

#include <vector>
#include <memory>

#include "./typedefs.h"
#include "./tweet.h"
#include "./retweet.h"


extern QSqlDatabase DB;

using std::vector;
using std::shared_ptr;


class Tweet;  // Forward declaration


class User {
 public:
    UserID id;
    QString display_name;
    QString handle;
    QString bio;
    int following_count;
    int followers_count;
    QString location;
    QString website;
    int join_date;
    bool is_private;
    bool is_verified;
    bool is_banned;

    TweetID pinned_tweet_id;
    shared_ptr<Tweet> pinned_tweet = nullptr;

    bool is_followed;
    bool is_content_downloaded;

    static User get_user_by_handle(UserHandle);
    static User get_user_by_id(UserID);
    static vector<User> get_followed_users();

    void reload_from_db();

    QString get_profile_image_url() const;
    QString get_profile_image_local_path() const;
    QString get_banner_image_url() const;
    QString get_banner_image_local_path() const;
    QString get_permalink() const;
    bool has_default_profile_image() const;

    vector<Tweet> get_tweets(bool include_replies = true);
    vector<Tweet> get_tweets(int min_posted_at_time, int max_posted_at_time, bool include_replies = true);
    vector<Retweet> get_retweets();
    vector<Retweet> get_retweets(int min_retweeted_at_time, int max_retweeted_at_time);

    // Engine calls
    int download_new_tweets();
    int follow();
    int unfollow();
    static int fetch_user(UserHandle);

    static const QString SQL_USER_FIELDS;
    static void extract_user_from_query_row(QSqlQuery, User*);

    User();

 private:
    // Private because there's a `/profile_images/` prefix that can be added
    QString profile_image_url;
    QString profile_image_local_path;
    QString banner_image_url;
    QString banner_image_local_path;
};

#endif  // USER_H
