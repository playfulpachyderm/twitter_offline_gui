#ifndef RETWEET_H
#define RETWEET_H

#include <QSqlQuery>
#include <QString>

#include <memory>
#include <vector>

#include "./typedefs.h"
#include "./tweet.h"

using std::shared_ptr;
using std::make_shared;


class User;  // Forward declaration
namespace SSF {
    class Cursor; // Forward declaration
}

class Retweet {
 public:
    RetweetID id;
    TweetID tweet_id;
    UserID retweeted_by_id;
    int retweeted_at;

    shared_ptr<Tweet> get_tweet();
    shared_ptr<User> get_user();

    static Retweet get_retweet_by_id(RetweetID);
    static vector<Retweet> get_followed_retweets_since(int min_retweeted_at, int max_retweeted_at);

 private:
    Retweet();

    shared_ptr<Tweet> tweet = nullptr;
    shared_ptr<User> user = nullptr;

    static Retweet extract_retweet_from_query_row(QSqlQuery);
    static const QString SQL_RETWEET_FIELDS;

    friend class User;
    friend class SSF::Cursor;
};

#endif  // RETWEET_H
