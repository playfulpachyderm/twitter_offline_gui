#include "search.h"

#include <QSqlQuery>
#include <QVariant>

#include "../_db.h"

SearchParams SearchParams::parse_search_query(QString query, bool* is_ok) {
    SearchParams ret;
    *is_ok = true;  // Every query is OK until proven otherwise

    QString current_token = "";
    bool is_in_quotes = false;

    for (QString letter: query) {
        // If there's a space, check for a completed token
        if (letter == " " && !is_in_quotes) {
            // If the token is empty, ignore it
            if (current_token == "") {
                continue;
            }
            ret.update_with_token(current_token, is_ok);
            current_token = "";
            continue;
        }

        // Handle quotes
        if (letter == "\"") {
            if (is_in_quotes) {
                is_in_quotes = false;
                ret.update_with_token(current_token, is_ok);
                current_token = "";
                continue;
            } else {
                is_in_quotes = true;
                continue;
            }
        }

        // Handle regular letters
        current_token += letter;
    }
    if (current_token != "") {
        ret.update_with_token(current_token, is_ok);
    }

    if (is_in_quotes) {
        // If quotes are not matched, it's not a valid query
        *is_ok = false;
    }
    ret.original_query = query;
    return ret;
}

void SearchParams::update_with_token(QString token, bool* is_ok) {
    if (token.startsWith("from:")) {
        this->from_user = token.mid(5);
        return;
    }
    if (token.startsWith("to:")) {
        this->to_users.push_back(token.mid(3));
        return;
    }

    if (token.startsWith("since:")) {
        this->timestamp_from = yyyymmdd_to_timestamp(token.mid(6));
        if (this->timestamp_from == -1) {
            *is_ok = false;
        }
        return;
    }
    if (token.startsWith("until:")) {
        this->timestamp_to = yyyymmdd_to_timestamp(token.mid(6));
        if (this->timestamp_to == -1) {
            *is_ok = false;
        }
        return;
    }
    if (token.startsWith("retweeted_by:")) {
        this->retweeted_by_user = token.mid(13);
        return;
    }
    if (token == "filter:links") {
        this->filter_links = true;
        return;
    }
    if (token == "filter:images") {
        this->filter_images = true;
        return;
    }
    if (token == "filter:videos") {
        this->filter_videos = true;
        return;
    }

    this->keywords << token;
}

/**
 * Execute the query and return the resulting Tweets
 */
vector<Tweet> SearchParams::execute() const {
    vector<Tweet> ret;
    QSqlQuery query(DB);

    QString tables = "tweets";
    if (this->retweeted_by_user != "") {
        tables += " join retweets on tweets.id = retweets.tweet_id";
    }

    QString query_text = "select " + Tweet::SQL_TWEET_FIELDS + " from " + tables + " where ";

    QStringList where_clauses;
    vector<QVariant> bind_values;

    for (QString token: this->keywords) {
        where_clauses << "text like ?";
        bind_values.push_back("%" + token + "%");
    }
    if (this->from_user != "") {
        where_clauses << "user_id = (select id from users where handle like ?)";
        bind_values.push_back(this->from_user);
    }
    if (this->retweeted_by_user != "") {
        where_clauses << "retweeted_by = (select id from users where handle like ?)";
        bind_values.push_back(this->retweeted_by_user);
    }
    for (QString to_user: this->to_users) {
        where_clauses << "reply_mentions like ?";
        bind_values.push_back("%" + to_user + "%");
        printf("Adding to_user: %s\n",  to_user.toStdString().c_str());
    }
    if (this->timestamp_to != 0) {
        where_clauses << "posted_at < ?";
        bind_values.push_back(this->timestamp_to);
    }
    if (this->timestamp_from != 0) {
        where_clauses << "posted_at > ?";
        bind_values.push_back(this->timestamp_from);
    }
    if (this->filter_links) {
        where_clauses << "exists (select 1 from urls where urls.tweet_id = tweets.id)";
    }
    if (this->filter_images) {
        where_clauses << "exists (select 1 from images where images.tweet_id = tweets.id)";
    }
    if (this->filter_videos) {
        where_clauses << "exists (select 1 from videos where videos.tweet_id = tweets.id)";
    }

    query_text += where_clauses.join(" and ");
    query_text += " order by num_likes desc limit 200";  // TODO: paginate this

    if (!query.prepare(query_text)) {
        throw DBException("Error preparing query");
    }

    for (QVariant value: bind_values) {
        query.addBindValue(value);
    }
    if (!query.exec()) {
        throw DBException("Error executing query: \n" + query_text);
    }

    while (query.next()) {
        Tweet t;
        Tweet::extract_tweet_from_query_row(query, &t);
        Tweet::add_extras_to_tweet(&t);
        ret.push_back(t);
    }
    return ret;
}

QString SearchParams::get_original_query() const {
    return original_query;
}


int yyyymmdd_to_timestamp(QString yyyymmdd) {
    QDateTime result = QDateTime::fromString(yyyymmdd, "yyyy-MM-dd");
    if (!result.isValid()) {
        return -1;
    }
    return result.toSecsSinceEpoch();
}
