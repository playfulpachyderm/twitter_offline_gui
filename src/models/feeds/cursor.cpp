#include "./cursor.h"

#include "../_db.h"


Feed SSF::Cursor::get_next_page() const {
    QSqlQuery query(DB);
    bool is_retweets = search_params.retweeted_by_user != "";

    QString tables = "tweets";
    if (is_retweets) {
        tables += " join retweets on tweets.id = retweets.tweet_id";
    }

    QStringList where_clauses;
    vector<QVariant> bind_values;
    for (QString token: search_params.keywords) {
        where_clauses << "text like ?";
        bind_values.push_back("%" + token + "%");
    }
    if (search_params.from_user != "") {
        where_clauses << "user_id = (select id from users where handle like ?)";
        bind_values.push_back(search_params.from_user);
    }
    if (is_retweets) {
        where_clauses << "retweeted_by = (select id from users where handle like ?)";
        bind_values.push_back(search_params.retweeted_by_user);
    }
    for (QString to_user: search_params.to_users) {
        where_clauses << "reply_mentions like ?";
        bind_values.push_back("%" + to_user + "%");
        printf("Adding to_user: %s\n",  to_user.toStdString().c_str());
    }
    if (search_params.timestamp_to != 0) {
        where_clauses << "posted_at < ?";
        bind_values.push_back(search_params.timestamp_to);
    }
    if (search_params.timestamp_from != 0) {
        where_clauses << "posted_at > ?";
        bind_values.push_back(search_params.timestamp_from);
    }
    if (search_params.filter_links) {
        where_clauses << "exists (select 1 from urls where urls.tweet_id = tweets.id)";
    }
    if (search_params.filter_images) {
        where_clauses << "exists (select 1 from images where images.tweet_id = tweets.id)";
    }
    if (search_params.filter_videos) {
        where_clauses << "exists (select 1 from videos where videos.tweet_id = tweets.id)";
    }

    QString order_by_clause;
    QString pagination_where_clause;
    if (this->sort_order == SSF::SORT_ORDER_NEWEST) {
        order_by_clause = is_retweets ? "order by retweeted_at desc" : "order by posted_at desc";
        pagination_where_clause = is_retweets ? "retweeted_at < ?" : "posted_at < ?";
    } else if (this->sort_order == SSF::SORT_ORDER_OLDEST) {
        order_by_clause = is_retweets ? "order by retweeted_at asc" : "order by posted_at asc";
        pagination_where_clause = is_retweets ? "retweeted_at > ?" : "posted_at > ?";
    } else if (this->sort_order == SSF::SORT_ORDER_MOST_LIKES) {
        order_by_clause = "order by num_likes desc";
        pagination_where_clause = "num_likes < ?";
    } else if (this->sort_order == SSF::SORT_ORDER_MOST_RETWEETS) {
        order_by_clause = "order by num_retweets desc";
        pagination_where_clause = "num_retweets < ?";
    } else {
        throw DBException(this->sort_order);
    }

    // Paginate the query, unless it's the top of the feed
    if (this->cursor_type != SSF::CursorType::START) {
        where_clauses << pagination_where_clause;
        bind_values.push_back(this->cursor_position);
    }

    QString select_fields = is_retweets ? Retweet::SQL_RETWEET_FIELDS : Tweet::SQL_TWEET_FIELDS;

    QString query_text = "select " + select_fields +
                         " from " + tables +
                         " where " + where_clauses.join(" and ") +
                         " " + order_by_clause + " " +
                         QString("limit %1").arg(this->page_size);

    if (!query.prepare(query_text)) {
        throw DBException(QString("Error preparing query: %1").arg(query_text));
    }

    for (QVariant value: bind_values) {
        query.addBindValue(value);
    }
    if (!query.exec()) {
        throw DBException("Error executing query: \n" + query_text);
    }

    vector<Retweet> retweets;
    vector<Tweet> tweets;
    int new_cursor_position;
    if (is_retweets) {
        while (query.next()) {
            retweets.push_back(Retweet::extract_retweet_from_query_row(query));
        }
        if (retweets.size() > 0) {
            if (this->sort_order == SSF::SORT_ORDER_NEWEST || this->sort_order == SSF::SORT_ORDER_OLDEST)
                new_cursor_position = retweets.back().retweeted_at;
            else if (this->sort_order == SSF::SORT_ORDER_MOST_LIKES)
                new_cursor_position = retweets.back().get_tweet()->num_likes;
            else if (this->sort_order == SSF::SORT_ORDER_MOST_RETWEETS)
                new_cursor_position = retweets.back().get_tweet()->num_retweets;
            else throw DBException(this->sort_order);
        }
    } else {
        while (query.next()) {
            Tweet t;
            Tweet::extract_tweet_from_query_row(query, &t);
            Tweet::add_extras_to_tweet(&t);
            tweets.push_back(t);
        }
        if (tweets.size() > 0) {
            if (this->sort_order == SSF::SORT_ORDER_NEWEST || this->sort_order == SSF::SORT_ORDER_OLDEST)
                new_cursor_position = tweets.back().posted_at;
            else if (this->sort_order == SSF::SORT_ORDER_MOST_LIKES)
                new_cursor_position = tweets.back().num_likes;
            else if (this->sort_order == SSF::SORT_ORDER_MOST_RETWEETS)
                new_cursor_position = tweets.back().num_retweets;
            else throw DBException(this->sort_order);
        }
    }

    // Construct the feed
    Feed ret = Feed::construct_from(tweets, retweets);

    // Construct the new cursor
    SSF::Cursor new_cursor;
    new_cursor.sort_order = this->sort_order;
    new_cursor.search_params = this->search_params;
    if (retweets.size() + tweets.size() < this->page_size) {
        new_cursor.cursor_type = SSF::CursorType::END;
    } else {
        new_cursor.cursor_type = SSF::CursorType::MIDDLE;
    }
    new_cursor.cursor_position = new_cursor_position;
    new_cursor.page_size = this->page_size;

    ret.cursor = make_shared<SSF::Cursor>(new_cursor);
    return ret;
}
