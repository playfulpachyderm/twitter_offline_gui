#ifndef SPACE_H
#define SPACE_H

#include <QString>
#include <QVector>

#include <memory>

#include "typedefs.h"

using std::shared_ptr;

class User; // Forward declaration


class Space {
 public:
    SpaceID id;
    QString short_url;
    QString state;
    QString title;

    int created_at;
    int started_at;
    int ended_at;
    int updated_at;

    UserID created_by_id = 0;
    shared_ptr<User> created_by_user = nullptr;
    QVector<shared_ptr<User>> participants;

    int live_listeners_count;
    bool is_available_for_replay;
    int replay_watch_count;
    bool is_details_fetched;

    static Space get_space_by_id(SpaceID);
    static const QString SQL_SPACE_FIELDS;
};

#endif  // SPACE_H
