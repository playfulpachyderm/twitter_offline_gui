#ifndef TYPEDEFS_H
#define TYPEDEFS_H

#include <QString>

typedef uint64_t UserID;
typedef QString UserHandle;

typedef uint64_t ImageID;
typedef uint64_t VideoID;
typedef uint64_t TweetID;
typedef uint64_t RetweetID;
typedef uint64_t PollID;
typedef QString  SpaceID;


#endif  // TYPEDEFS_H
