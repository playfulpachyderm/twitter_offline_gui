#include "space.h"

#include <QSqlQuery>
#include <QSqlError>
#include <QVariant>

#include <memory>

#include "./_db.h"
#include "./user.h"

using std::make_shared;

const QString Space::SQL_SPACE_FIELDS = "id, created_by_id, short_url, state, title, created_at/1000, started_at/1000, ended_at/1000, updated_at/1000, is_available_for_replay, replay_watch_count, live_listeners_count, is_details_fetched";


Space Space::get_space_by_id(SpaceID id) {
    QSqlQuery query(DB);
    query.prepare("select " + Space::SQL_SPACE_FIELDS + " from spaces where id = :id");
    query.bindValue(":id", id);
    if (!query.exec()) {
        printf("Failed to execute!\n");
        printf("%s\n", query.lastError().text().toStdString().c_str());
        throw DBException("Query execution failure");
    }

    if (!query.next()) {
        printf("Last query: %s\n", query.lastQuery().toStdString().c_str());
        printf("Last error: %s\n", query.lastError().text().toStdString().c_str());
        throw DBException("Could not find space with id: " + id);
    }

    Space ret;
    ret.id = query.value(0).toString();
    ret.created_by_id = query.value(1).toULongLong();
    ret.short_url = query.value(2).toString();
    ret.state = query.value(3).toString();
    ret.title = query.value(4).toString();
    ret.created_at = query.value(5).toInt();
    ret.started_at = query.value(6).toInt();
    ret.ended_at = query.value(7).toInt();
    ret.updated_at = query.value(8).toInt();
    ret.is_available_for_replay = query.value(9).toBool();
    ret.replay_watch_count = query.value(10).toInt();
    ret.live_listeners_count = query.value(11).toInt();
    ret.is_details_fetched = query.value(12).toBool();

    ret.created_by_user = make_shared<User>(User::get_user_by_id(ret.created_by_id));

    // Fetch space participants
    QSqlQuery query2;
    // TODO: remove the "distinct" after doing a migration to remove the duplicates
    query2.prepare("select distinct " + User::SQL_USER_FIELDS + ", user_id from space_participants left join users on users.id = user_id where space_id = :id");
    query2.bindValue(":id", id);
    if (!query2.exec()) {
        printf("Failed to execute!\n");
        printf("Last query: %s\n", query2.lastQuery().toStdString().c_str());
        printf("Last error: %s\n", query2.lastError().text().toStdString().c_str());
        throw DBException("Query2 execution failure");
    }

    while (query2.next()) {
        User u;
        User::extract_user_from_query_row(query2, &u);
        if (u.id == 0) {
            // Unfetched user; keep the user ID from the space_participants table
            u.id = query2.value("user_id").toULongLong();
        }
        ret.participants.push_back(make_shared<User>(u));
    }

    return ret;
}
