#include "global_state.h"

#include "./img_utils.h"

using std::make_shared;


QString GlobalState::PROFILE_DIR = "";
map<UserHandle, shared_ptr<QPixmap>> GlobalState::USER_AVATARS = {};
Engine GlobalState::TWITTER_ENGINE = Engine();
QString GlobalState::ENGINE_VERSION = "";

int GlobalState::USER_FEED_TWEET_BATCH_SIZE = 50;

shared_ptr<QPixmap> GlobalState::get_profile_img(shared_ptr<User> u) {
    if (USER_AVATARS.count(u->handle) == 0) {
        shared_ptr<QPixmap> result = make_shared<QPixmap>(QPixmap(
            PROFILE_DIR + u->get_profile_image_local_path()
        ));
        if (result->isNull()) {
            printf("User profile image could not be loaded for some reason! => @%s\n", u->handle.toStdString().c_str());
            return result;
        }
        circlify(result);
        USER_AVATARS[u->handle] = result;
    }

    return USER_AVATARS[u->handle];
}

/**
 * Clear cached version of this user's profile image.  Mainly for when user content gets downloaded,
 * so it needs to be upgraded from tiny image to regular one
 */
void GlobalState::delete_cached_profile_img(shared_ptr<User> u) {
    USER_AVATARS.erase(u->handle);
}
