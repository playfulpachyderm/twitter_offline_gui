#ifndef USER_FEED_HEADER_H
#define USER_FEED_HEADER_H

#include <QWidget>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QPixmap>
#include <QLabel>
#include <QResizeEvent>
#include <QPaintEvent>
#include <QContextMenuEvent>
#include <QPushButton>

#include <memory>

#include "../models/user.h"

#include "./components/dropdown_button.h"
#include "./author_info.h"


using std::shared_ptr;

class UserFeedHeader : public QWidget {
    Q_OBJECT

    private:
        shared_ptr<User> u;

        QVBoxLayout* layout;

        QPixmap _banner_image_pixmap;
        QLabel* banner_image_label;

        QVBoxLayout* everything_else_layout;

        QHBoxLayout* author_download_layout;
        AuthorInfo* author_info_panel;
        QPushButton* follow_button;
        DropdownButton* download_button;

        QLabel* bio_label;
        QLabel* location_label;
        QLabel* website_label;
        QLabel* joined_at_label;

        QHBoxLayout* follower_layout;
        QLabel* followers_count_label;
        QLabel* following_count_label;

        static QString format_joined_at(int join_date);

    private slots:
        void follow_button_clicked();
        void update_follow_button(bool is_followed);
        void open_in_external();
        void copy_user_handle();

    protected:
        void resizeEvent(QResizeEvent*) override;
        void paintEvent(QPaintEvent*) override;
        void contextMenuEvent(QContextMenuEvent*) override;

    public:
        explicit UserFeedHeader(QWidget* parent = nullptr, shared_ptr<User> u = nullptr);
};


#endif  // USER_FEED_HEADER_H
