#include "embedded_link.h"

#include <QPixmap>
#include <QUrl>
#include <QPalette>
#include <QColor>
#include <QFont>
#include <QSizePolicy>
#include <QDesktopServices>
#include <QMenu>
#include <QGuiApplication>
#include <QClipboard>
#include <QIcon>

#include "../lib/global_state.h"
#include "./utils/drawing.h"
#include "./colors.h"

EmbeddedLink::EmbeddedLink(QWidget* parent, shared_ptr<Url> url): QWidget(parent) {
	this->url = url;

	layout = new QVBoxLayout(this);
	layout->setContentsMargins(20,20,20,20);

	setCursor(Qt::PointingHandCursor);
	setToolTip(url->text);

	setSizePolicy(QSizePolicy(QSizePolicy::Maximum, QSizePolicy::Maximum));

	QPixmap preview_pixmap(GlobalState::PROFILE_DIR + url->get_thumbnail_path());
	link_preview_image = new QLabel(this);
	link_preview_image->setPixmap(preview_pixmap);
	// link_preview_image->setFixedSize(url->thumbnail_width, url->thumbnail_height);  // TODO: when uncommented, the thumbnail image paints outside of its fixed size and covers up the article title. Dunno why
	layout->addWidget(link_preview_image);

	title_label = new QLabel(this);
	title_label->setText(url->title);
	title_label->setWordWrap(true);
	if (!url->has_card) {
		title_label->setText(url->text);
		title_label->setWordWrap(true);
	}
	layout->addWidget(title_label);

	description_label = new QLabel(this);
	description_label->setText(url->description);
	description_label->setWordWrap(true);
	QFont f = font();
	f.setPixelSize(f.pixelSize() * 5/6);
	description_label->setFont(f);
	QPalette pal = palette();
	pal.setColor(QPalette::WindowText, QColor(100, 100, 100));
	description_label->setPalette(pal);
	layout->addWidget(description_label);

	domain_layout = new QHBoxLayout();
	layout->addLayout(domain_layout);

	QPixmap link_icon_pixmap(":/icons/link_icon.png");
	link_icon_label = new QLabel(this);
	link_icon_label->setPixmap(link_icon_pixmap);
	link_icon_label->setFixedSize(20, 20);
	link_icon_label->setScaledContents(true);
	domain_layout->addWidget(link_icon_label);

	domain_label = new QLabel(this);
	domain_label->setText(url->domain);
	domain_label->setWordWrap(true);
	domain_layout->addWidget(domain_label);

	connect(this, SIGNAL(clicked()), this, SLOT(open_in_browser()));
}

void EmbeddedLink::paintEvent(QPaintEvent*) {
	fill_background(this, COLOR_TWITTER_OFF_WHITE, 20);
	draw_outline(this, 20);
}

void EmbeddedLink::mousePressEvent(QMouseEvent* e) {
	if (e->button() == Qt::LeftButton) {
		emit clicked();
	} else {
		e->ignore();
	}
}

/**
 * Open this link in the user's system default browser
 */
void EmbeddedLink::open_in_browser() {
	// Ignore the click if we are running tests
	#ifndef TESTING_MODE
		QDesktopServices::openUrl(QUrl(url->text));
	#endif
}

void EmbeddedLink::contextMenuEvent(QContextMenuEvent* e) {
	e->accept();
	QMenu menu(this);
	menu.addAction(QIcon(":/icons/link_icon.png"), "Copy link", this, SLOT(copy_link()));
	menu.addSeparator();
	menu.addAction(QIcon(":/icons/open_external_icon.png"), "Open in browser", this, SLOT(open_in_browser()));
	menu.exec(e->globalPos());
}

void EmbeddedLink::copy_link() {
	QGuiApplication::clipboard()->setText(url->text);
}
