#include "clickable_icon.h"

#include <Qt>

ClickableIcon::ClickableIcon(QString icon_path, QString tooltip, QWidget* parent): QLabel(parent) {
	this->tooltip = tooltip;

	pixmap = QPixmap(icon_path);
	setPixmap(pixmap);
	setToolTip(this->tooltip);
	setFixedSize(30, 30);
	setScaledContents(true);
	setCursor(Qt::PointingHandCursor);
}

void ClickableIcon::mousePressEvent(QMouseEvent* e) {
	if (e->button() == Qt::LeftButton) {
		emit clicked();
	} else {
		e->ignore();
	}
}
