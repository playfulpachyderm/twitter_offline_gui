#ifndef LARGE_BUTTON_H
#define LARGE_BUTTON_H

#include <QWidget>
#include <QPushButton>
#include <QVBoxLayout>
#include <QLabel>

class LargeButton : public QPushButton {
    Q_OBJECT

 private:
    QVBoxLayout* layout;

    QLabel* label;
    QLabel* icon_label;

 public:
    explicit LargeButton(QString text, QString icon_path, QWidget* parent = nullptr);
};

#endif  // LARGE_BUTTON_H
