#include "large_button.h"

LargeButton::LargeButton(QString text, QString icon_path, QWidget* parent): QPushButton(parent) {
	layout = new QVBoxLayout(this);

	setFixedSize(250, 250);

	icon_label = new QLabel(this);
	icon_label->setPixmap(QPixmap(icon_path));
	icon_label->setScaledContents(true);
	layout->addWidget(icon_label);

	label = new QLabel(this);
	label->setText(text);
	layout->addWidget(label);
}
