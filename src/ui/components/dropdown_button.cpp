#include "dropdown_button.h"

#include <QStylePainter>
#include <QStyleOptionToolButton>

DropdownButton::DropdownButton(QWidget* parent): QToolButton(parent) {
	setPopupMode(QToolButton::InstantPopup);
}

/**
 * Courtesy of: https://bugreports.qt.io/browse/QTBUG-2036
 */
void DropdownButton::paintEvent(QPaintEvent*) {
	QPainter p(this);
	QPixmap icon_pixmap = icon().pixmap(iconSize());
	p.drawPixmap((width() - iconSize().width()) / 2, (height() - iconSize().height()) / 2, icon_pixmap);
	// QStylePainter p(this);
	// QStyleOptionToolButton opt;
	// initStyleOption(&opt);
	// opt.features &= (~QStyleOptionToolButton::HasMenu);
	// p.drawComplexControl(QStyle::CC_ToolButton, opt);
}
