#ifndef UI_COMPONENTS_TWEET_FEED_H
#define UI_COMPONENTS_TWEET_FEED_H

#include <QWidget>
#include <QVBoxLayout>
#include <QPushButton>

#include "../../models/feeds/feed.h"

#include "../column.h"
#include "../tweet_widget.h"


class TweetFeed : public QWidget {
    Q_OBJECT

 private:
    Column* column;
    QVBoxLayout* layout;
    QVBoxLayout* tweets_layout;
    vector<TweetWidget*> tweet_widgets;

    int oldest_timestamp;

    QPushButton* load_more_tweets_button;

 signals:
    void load_more_tweets();

 public:
    explicit TweetFeed(Feed feed, QWidget* parent = nullptr, Column* column = nullptr);

    void extend_with(Feed f);
    int get_oldest_timestamp();
    void set_load_more_button_enabled(bool is_enabled);
};

#endif  // UI_COMPONENTS_TWEET_FEED_H
