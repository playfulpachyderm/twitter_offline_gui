#include "navbar.h"

#include <QPalette>
#include <QPen>
#include <QPainter>

#include "./colors.h"


Navbar::Navbar(QWidget* parent): QWidget(parent) {
    QPalette pal = palette();
    pal.setColor(QPalette::Window, COLOR_TWITTER_OFF_WHITE);
    setPalette(pal);
    setAutoFillBackground(true);

    layout = new QHBoxLayout(this);
    layout->setContentsMargins(10, 10, 30, 10);
    layout->setSpacing(30);

    // TODO: disable when the navigation depth is 0
    back_button = new ClickableIcon(":/icons/back_button_icon", "Back", this);
    connect(back_button, SIGNAL(clicked()), parent, SLOT(go_back()));
    layout->addWidget(back_button);

    depth_label = new QLabel(this);
    depth_label->setText(QString::number(0));
    depth_label->setToolTip("How many navigation screens deep you are");
    depth_label->setSizePolicy(QSizePolicy(QSizePolicy::Maximum, QSizePolicy::Minimum));
    layout->addWidget(depth_label);

    search_box = new QLineEdit(this);
    search_box->setPlaceholderText("Search");
    search_box->setToolTip("Press <enter> to search");
    layout->addWidget(search_box);
    layout->setStretchFactor(search_box, 1);
    connect(search_box, SIGNAL(returnPressed()), this, SLOT(trigger_search()));

    home_button = new ClickableIcon(":/icons/home_icon", "Home", this);
    home_button->setFixedSize(32, 25);
    connect(home_button, SIGNAL(clicked()), this, SIGNAL(home_button_clicked()));
    layout->addWidget(home_button);

    list_followed_button = new ClickableIcon(":/icons/list_icon", "Show followed users", this);
    list_followed_button->setFixedSize(30, 30);
    connect(list_followed_button, SIGNAL(clicked()), this, SIGNAL(list_followed_button_clicked()));
    layout->addWidget(list_followed_button);
}

void Navbar::paintEvent(QPaintEvent*) {
    QPainter painter(this);
    QPen pen(COLOR_TWITTER_OFF_WHITE_DARK);
    painter.setPen(pen);

    painter.drawLine(0, height() - 1, width() - 1, height() - 1);
}


void Navbar::set_navigation_depth(int depth) {
    depth_label->setText(QString::number(depth));
}

void Navbar::trigger_search() {
    emit submit_search(search_box->text());
}
