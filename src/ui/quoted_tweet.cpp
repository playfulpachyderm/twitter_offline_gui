#include "quoted_tweet.h"

#include <Qt>
#include <QPixmap>
#include <QSizePolicy>
#include <QPainter>
#include <QPen>
#include <QColor>

#include "../models/tweet.h"
#include "../lib/global_state.h"

#include "./utils/drawing.h"

#include "./author_info.h"

QuotedTweet::QuotedTweet(QWidget* parent, shared_ptr<Tweet> t, Column* column, int nesting_limit): TweetWidget(parent, t, column, nullptr, nesting_limit) {}


void QuotedTweet::paintEvent(QPaintEvent*) {
    draw_outline(this, 20);
}
