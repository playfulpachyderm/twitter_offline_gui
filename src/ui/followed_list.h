#ifndef FOLLOWED_LIST_H
#define FOLLOWED_LIST_H

#include <QWidget>
#include <QScrollArea>
#include <QVBoxLayout>

#include "../models/user.h"
#include "./column.h"


class FollowedList : public QScrollArea {
    Q_OBJECT

 private:
    QWidget* dummy;
    Column* column;
    vector<User> followed_users;
    QVBoxLayout* layout;

 public:
    explicit FollowedList(Column* parent = nullptr);
};

#endif  // FOLLOWED_LIST_H
