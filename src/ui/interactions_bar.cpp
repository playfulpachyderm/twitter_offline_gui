#include "interactions_bar.h"

#include <QPixmap>
#include <QMenu>
#include <QSize>
#include <QIcon>

#include "./components/dropdown_button.h"

InteractionsBar::InteractionsBar(QWidget* parent, shared_ptr<Tweet> t): QWidget(parent) {
    this->t = t;

    layout = new QHBoxLayout(this);
    layout->setContentsMargins(0, 0, 0, 0);

    QPixmap like_pixmap(":/icons/like_icon.png");
    QPixmap retweet_pixmap(":/icons/retweet_icon.png");
    QPixmap reply_pixmap(":/icons/reply_icon.png");
    QPixmap quote_tweet_pixmap(":/icons/quote_marks_icon.png");

    likes_icon_label = new QLabel(this);
    likes_icon_label->setPixmap(like_pixmap);
    likes_icon_label->setFixedSize(30, 30);
    likes_icon_label->setScaledContents(true);
    likes_icon_label->setToolTip("Like");

    likes_count_label = new QLabel(this);
    likes_count_label->setText(QString::number(this->t->num_likes));

    retweets_icon_label = new QLabel(this);
    retweets_icon_label->setPixmap(retweet_pixmap);
    retweets_icon_label->setFixedSize(30, 30);
    retweets_icon_label->setScaledContents(true);
    retweets_icon_label->setToolTip("Retweet");

    retweets_count_label = new QLabel(this);
    retweets_count_label->setText(QString::number(this->t->num_retweets));

    replies_icon_label = new QLabel(this);
    replies_icon_label->setPixmap(reply_pixmap);
    replies_icon_label->setFixedSize(30, 30);
    replies_icon_label->setScaledContents(true);
    replies_icon_label->setToolTip("Reply");

    replies_count_label = new QLabel(this);
    replies_count_label->setText(QString::number(this->t->num_replies));

    quote_tweets_icon_label = new QLabel(this);
    quote_tweets_icon_label->setPixmap(quote_tweet_pixmap);
    quote_tweets_icon_label->setFixedSize(30, 30);
    quote_tweets_icon_label->setScaledContents(true);
    quote_tweets_icon_label->setToolTip("Quote-tweet");

    quote_tweets_count_label = new QLabel(this);
    quote_tweets_count_label->setText(QString::number(this->t->num_quote_tweets));

    dropdown_button = new DropdownButton(this);
    dropdown_button->setToolTip("Show download options for this tweet");
    dropdown_button->setIcon(QIcon(":/icons/dropdown_icon.png"));
    dropdown_button->setIconSize(QSize(30, 30));

    // DUPE tweet-context-menu
    QMenu* context_menu = new QMenu(this);
    context_menu->addAction(QIcon(":/icons/link_icon.png"), "Copy tweet URL", parent, SLOT(copy_link()));
    context_menu->addSeparator();
    context_menu->addAction("Download contents", parent, SLOT(download_contents()));
    context_menu->addAction("Download this conversation", parent, SLOT(download_conversation()));
    context_menu->addSeparator();
    context_menu->addAction(QIcon(":/icons/open_external_icon.png"), "Open tweet in browser", parent, SLOT(open_in_browser()));
    dropdown_button->setMenu(context_menu);

    layout->addWidget(quote_tweets_icon_label);
    layout->addWidget(quote_tweets_count_label);
    layout->addStretch(1);
    layout->addWidget(replies_icon_label);
    layout->addWidget(replies_count_label);
    layout->addStretch(1);
    layout->addWidget(retweets_icon_label);
    layout->addWidget(retweets_count_label);
    layout->addStretch(1);
    layout->addWidget(likes_icon_label);
    layout->addWidget(likes_count_label);
    layout->addStretch(3);
    layout->addWidget(dropdown_button);
}
