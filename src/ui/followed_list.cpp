#include "followed_list.h"

#include <QHBoxLayout>

#include <memory>

#include "./author_info.h"

using std::make_shared;

FollowedList::FollowedList(Column* parent): QScrollArea(parent) {
	this->column = parent;

	setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOn);
	setWidgetResizable(true);
	setFrameShape(QFrame::NoFrame);

	dummy = new QWidget(this);
	layout = new QVBoxLayout(dummy);
	layout->setSpacing(30);
	dummy->setLayout(layout);  // TODO: unnecessary??

	followed_users = User::get_followed_users();

	for (User u: followed_users) {
		QHBoxLayout* entry = new QHBoxLayout();
		layout->addLayout(entry);

		AuthorInfo* author_info_panel = new AuthorInfo(dummy, make_shared<User>(u));
		author_info_panel->set_image_size(80);
		author_info_panel->set_text_size(18);
		connect(author_info_panel, SIGNAL(profile_image_clicked(shared_ptr<User>)), parent, SLOT(open_user_feed(shared_ptr<User>)));
		entry->addWidget(author_info_panel);

		entry->addStretch(1);
	}

	layout->addStretch(1);
	setWidget(dummy);
}
