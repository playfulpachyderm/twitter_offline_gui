#include "user_feed_header.h"

#include <Qt>
#include <QPen>
#include <QPainter>
#include <QFont>
#include <QPalette>
#include <QMenu>
#include <QStyleFactory>
#include <QDesktopServices>
#include <QUrl>
#include <QGuiApplication>
#include <QClipboard>
#include <QDateTime>
#include <QIcon>

#include "../lib/global_state.h"
#include "./components/image_widget.h"
#include "./utils/clickable_entities.h"

#include "./colors.h"


UserFeedHeader::UserFeedHeader(QWidget* parent, shared_ptr<User> u): QWidget(parent) {
    this->u = u;

    layout = new QVBoxLayout(this);
    layout->setContentsMargins(0,0,0,0);
    layout->setSpacing(20);

    QFont the_font("Titillium Web");
    the_font.setPixelSize(14);
    setFont(the_font);

    QPalette gray_pal = palette();
    gray_pal.setColor(QPalette::WindowText, QColor(150, 150, 150));  // TODO: colors


    _banner_image_pixmap = QPixmap(GlobalState::PROFILE_DIR + u->get_banner_image_local_path());

    banner_image_label = new ImageWidget(this, GlobalState::PROFILE_DIR + u->get_banner_image_local_path());
    layout->addWidget(banner_image_label);

    everything_else_layout = new QVBoxLayout();
    layout->addLayout(everything_else_layout);
    everything_else_layout->setContentsMargins(60, 20, 60, 20);
    everything_else_layout->setSpacing(8);

    author_download_layout = new QHBoxLayout();
    everything_else_layout->addLayout(author_download_layout);
    author_download_layout->setContentsMargins(0,0,0,0);

    author_info_panel = new AuthorInfo(this, u);
    author_info_panel->set_image_size(140);
    author_info_panel->set_text_size(18);

    follow_button = new QPushButton(this);
    update_follow_button(u->is_followed);  // Initializes the text, background color
    follow_button->setStyle(QStyleFactory::create("fusion"));
    connect(follow_button, SIGNAL(clicked()), this, SLOT(follow_button_clicked()));

    download_button = new DropdownButton(this);
    download_button->setToolTip("Show download options for this user");
    download_button->setIcon(QIcon(":/icons/download_icon.png"));
    download_button->setIconSize(QSize(50, 50));

    QMenu* download_menu = new QMenu(this);
    if (parent != nullptr) {
        download_menu->addAction("Download new tweets", parent->parentWidget(), SLOT(download_new_tweets()));
        download_menu->addAction("Download profile pics", parent->parentWidget(), SLOT(download_user_content()));
    }
    download_button->setMenu(download_menu);

    author_download_layout->addWidget(author_info_panel);
    author_download_layout->addStretch(1);
    author_download_layout->addWidget(follow_button);
    author_download_layout->addWidget(download_button);

    QPalette info_palette = palette();
    info_palette.setColor(QPalette::WindowText, COLOR_TWITTER_TEXT_GRAY);

    // Bio
    if (u->bio != "") {
        QHBoxLayout* bio_layout = new QHBoxLayout();
        bio_layout->setContentsMargins(0, 10, 0, 10);
        everything_else_layout->addLayout(bio_layout);
        bio_label = new QLabel(this);
        bio_label->setText(get_rich_text_for(u->bio));
        if (parent != nullptr && parent->parentWidget() != nullptr) {
            connect(bio_label, SIGNAL(linkActivated(QString)), parent->parentWidget()->parentWidget(), SLOT(entity_clicked(QString)));
        }
        bio_label->setWordWrap(true);
        bio_label->setTextInteractionFlags(Qt::TextSelectableByMouse | Qt::LinksAccessibleByMouse);
        bio_label->setContextMenuPolicy(Qt::NoContextMenu);
        bio_layout->addWidget(bio_label);
    }

    // Location
    if (u->location != "") {
        QHBoxLayout* location_layout = new QHBoxLayout();
        location_layout->setContentsMargins(0,0,0,0);
        everything_else_layout->addLayout(location_layout);

        QLabel* location_icon = new QLabel(this);
        location_icon->setPixmap(QPixmap(":/icons/location_icon.png"));
        location_icon->setFixedSize(25, 25);
        location_icon->setScaledContents(true);
        location_layout->addWidget(location_icon);

        location_label = new QLabel(this);
        location_label->setText(u->location);
        location_label->setTextInteractionFlags(Qt::TextSelectableByMouse);
        location_label->setContextMenuPolicy(Qt::NoContextMenu);
        location_label->setPalette(info_palette);
        location_layout->addWidget(location_label);
    }

    // Website
    if (u->website != "") {
        QHBoxLayout* website_layout = new QHBoxLayout();
        website_layout->setContentsMargins(0,0,0,0);
        everything_else_layout->addLayout(website_layout);

        QLabel* website_icon = new QLabel(this);
        website_icon->setPixmap(QPixmap(":/icons/link_icon.png"));
        website_icon->setFixedSize(25, 25);
        website_icon->setScaledContents(true);
        website_layout->addWidget(website_icon);

        website_label = new QLabel(this);
        QString website_rich_text = QString("<a href='%1' style='%2'>%1</a>")
            .arg(u->website)
            .arg("text-decoration: none; color: rgb(27,149,224)");  // TODO: put this in a constant somewhere (it's used several places)
        website_label->setText(website_rich_text);
        website_label->setTextInteractionFlags(Qt::TextSelectableByMouse | Qt::LinksAccessibleByMouse);
        website_label->setOpenExternalLinks(true);
        website_label->setContextMenuPolicy(Qt::NoContextMenu);
        website_layout->addWidget(website_label);
    }

    // Join date
    QHBoxLayout* joined_at_layout = new QHBoxLayout();
    joined_at_layout->setContentsMargins(0,0,0,0);
    everything_else_layout->addLayout(joined_at_layout);

    QLabel* joined_at_icon = new QLabel(this);
    QPixmap joined_at_pixmap(":/icons/calendar_icon.png");
    joined_at_icon->setPixmap(joined_at_pixmap);
    joined_at_icon->setFixedSize(25, 25);
    joined_at_icon->setScaledContents(true);
    joined_at_layout->addWidget(joined_at_icon);

    joined_at_label = new QLabel(this);
    joined_at_label->setText("Joined: " + UserFeedHeader::format_joined_at(u->join_date));
    joined_at_label->setTextInteractionFlags(Qt::TextSelectableByMouse);
    joined_at_label->setContextMenuPolicy(Qt::NoContextMenu);
    joined_at_label->setPalette(info_palette);
    joined_at_layout->addWidget(joined_at_label);


    // Followers
    follower_layout = new QHBoxLayout();
    everything_else_layout->addLayout(follower_layout);
    follower_layout->setSpacing(10);
    follower_layout->setContentsMargins(0, 10, 0, 10);

    the_font.setWeight(80);

    followers_count_label = new QLabel(this);
    followers_count_label->setText(QString::number(u->followers_count));
    followers_count_label->setTextInteractionFlags(Qt::TextSelectableByMouse);
    followers_count_label->setContextMenuPolicy(Qt::NoContextMenu);
    followers_count_label->setFont(the_font);

    QLabel* following_label = new QLabel(this);
    following_label->setText("is following");
    following_label->setPalette(gray_pal);

    QLabel* followers_label = new QLabel(this);
    followers_label->setText("followers");
    followers_label->setPalette(gray_pal);

    following_count_label = new QLabel(this);
    following_count_label->setText(QString::number(u->following_count));
    following_count_label->setTextInteractionFlags(Qt::TextSelectableByMouse);
    following_count_label->setContextMenuPolicy(Qt::NoContextMenu);
    following_count_label->setFont(the_font);

    follower_layout->addWidget(followers_count_label);
    follower_layout->addWidget(followers_label);
    follower_layout->addStretch(1);
    follower_layout->addWidget(following_label);
    follower_layout->addWidget(following_count_label);
    follower_layout->addStretch(10);
}

void UserFeedHeader::resizeEvent(QResizeEvent*) {
    QPixmap banner_image_pixmap = _banner_image_pixmap.scaledToWidth(width(), Qt::SmoothTransformation);
    banner_image_label->setPixmap(banner_image_pixmap);
    banner_image_label->setFixedSize(banner_image_pixmap.size());
}

void UserFeedHeader::paintEvent(QPaintEvent*) {
    QPainter painter(this);
    QPen pen(COLOR_OUTLINE_GRAY);
    pen.setWidth(1);
    painter.setPen(pen);

    // Draw a double border at the bottom
    painter.drawLine(0, height() - 1, width() - 1, height() - 1);
    painter.drawLine(0, height() - 4, width() - 1, height() - 4);
}

void UserFeedHeader::follow_button_clicked() {
    follow_button->setDisabled(true);
    if (!u->is_followed) {
        int result = u->follow();
        if (result == 0) {
            update_follow_button(true);
        }
    } else {
        int result = u->unfollow();
        if (result == 0) {
            update_follow_button(false);
        }
    }
    follow_button->setDisabled(false);
}

void UserFeedHeader::update_follow_button(bool is_followed) {
    follow_button->setText(u->is_followed ? "Following" : "Follow");

    QPalette pal = follow_button->palette();
    pal.setColor(follow_button->backgroundRole(), is_followed ? COLOR_BACKGROUND_GREEN : COLOR_TWITTER_OFF_WHITE_DARK);
    follow_button->setPalette(pal);
}

void UserFeedHeader::contextMenuEvent(QContextMenuEvent* e) {
    QMenu menu(this);
    menu.addAction(QIcon(":/icons/link_icon.png"), "Copy user handle", this, SLOT(copy_user_handle()));
    menu.addSeparator();
    menu.addAction(QIcon(":/icons/open_external_icon.png"), "Open user feed in browser", this, SLOT(open_in_external()));
    menu.exec(e->globalPos());
}

void UserFeedHeader::open_in_external() {
    QDesktopServices::openUrl(QUrl(u->get_permalink()));
}
void UserFeedHeader::copy_user_handle() {
    QGuiApplication::clipboard()->setText(u->handle);
}

QString UserFeedHeader::format_joined_at(int join_date) {
    QDateTime joined_at_datetime = QDateTime::fromSecsSinceEpoch(join_date).toTimeZone(QTimeZone::utc());
    return joined_at_datetime.toString("MMM yyyy").remove(".");
}
