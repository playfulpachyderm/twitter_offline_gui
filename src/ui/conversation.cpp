#include "conversation.h"

#include <Qt>
#include <QFrame>

#include <memory>

#include "../models/tweet.h"
#include "../models/_db.h"

#include "./missing_tweet_placeholder.h"

using std::make_shared;
using std::shared_ptr;


Conversation::Conversation(Column* parent, shared_ptr<Tweet> t): QScrollArea(parent) {
    this->t = t;

    setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    setWidgetResizable(true);
    setFrameShape(QFrame::NoFrame);

    dummy = new QWidget(this);
    layout = new QVBoxLayout(dummy);
    layout->setSpacing(0);
    layout->setContentsMargins(0, 20, 0, 0);
    dummy->setLayout(layout);

    // The main tweet itself
    main_tweet = new TweetWidget(this, t, parent);
    main_tweet->set_is_focused(true);
    main_tweet->set_vertical_spacing(false, true);
    layout->addWidget(main_tweet);

    // Assemble the parent conversation thread
    shared_ptr<Tweet> tmp = t;
    while (tmp->in_reply_to_id != 0) {
        try {
            tmp = tmp->get_replied_tweet();
        } catch (DBException &e) {
            MissingTweetPlaceholder* placeholder = new MissingTweetPlaceholder(tmp->in_reply_to_id, this);
            layout->insertWidget(0, placeholder);
            break;
        }
        TweetWidget* t_widget = new TweetWidget(this, tmp, parent);
        t_widget->set_has_divider(false);
        t_widget->set_has_vertical_reply_line(true);
        t_widget->set_vertical_spacing(false, false);
        layout->insertWidget(0, t_widget);
    }

    // Assemble the thread below
    auto thread = t->get_thread();
    for (uint i = 0; i < thread.size(); i++) {
        TweetWidget* thread_w = new TweetWidget(this, thread[i], parent);
        bool is_first = i == 0;
        bool is_last  = i == thread.size() - 1;

        thread_w->set_has_divider(is_last);
        thread_w->set_has_vertical_reply_line(!is_last);
        thread_w->set_vertical_spacing(is_first, is_last);

        layout->addWidget(thread_w);
    }

    // Replies / comments
    for (shared_ptr<Tweet> reply: t->get_replies(20)) {
        // Exclude replies that are in the thread
        if (thread.size() > 0 && thread[0]->id == reply->id)
            continue;

        TweetWidget* reply_w = new TweetWidget(this, reply, parent);
        layout->addWidget(reply_w);
        vector<shared_ptr<Tweet>> reply_replies = reply->get_replies(1);
        if (reply_replies.size() > 0) {
            reply_w->set_has_divider(false);
            reply_w->set_has_vertical_reply_line(true);
            reply_w->set_vertical_spacing(true, false);

            TweetWidget* t_widget = new TweetWidget(this, reply_replies[0], parent);
            t_widget->set_vertical_spacing(false, false);
            layout->addWidget(t_widget);
        }
    }
    layout->addStretch(1);
    setWidget(dummy);

    // TODO: maybe do this in the calling method?  Might be not working because constructor didn't exit yet
    // ensureWidgetVisible(main_tweet, 0, -600);
}
