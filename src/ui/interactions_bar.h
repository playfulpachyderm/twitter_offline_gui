#ifndef INTERACTIONS_BAR_H
#define INTERACTIONS_BAR_H

#include <QWidget>
#include <QHBoxLayout>
#include <QLabel>

#include "../models/tweet.h"

#include "./components/clickable_icon.h"
#include "./components/dropdown_button.h"


class InteractionsBar : public QWidget {
    Q_OBJECT

 public:
    explicit InteractionsBar(QWidget* parent = nullptr, shared_ptr<Tweet> t = nullptr);

 private:
    shared_ptr<Tweet> t;

    QHBoxLayout* layout;

    QLabel* likes_icon_label;
    QLabel* likes_count_label;
    QLabel* retweets_icon_label;
    QLabel* retweets_count_label;
    QLabel* replies_icon_label;
    QLabel* replies_count_label;
    QLabel* quote_tweets_icon_label;
    QLabel* quote_tweets_count_label;

    DropdownButton* dropdown_button;

 signals:
    void clicked();
};

#endif  // INTERACTIONS_BAR_H
