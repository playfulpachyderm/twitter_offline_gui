#ifndef COLUMN_WIDGET_H
#define COLUMN_WIDGET_H

#include <QWidget>
#include <QString>
#include <QStackedLayout>
#include <QVBoxLayout>
#include <QMouseEvent>

#include <memory>
#include <vector>

#include "../models/tweet.h"
#include "../models/user.h"
#include "../models/feeds/search.h"

#include "./navbar.h"

using std::shared_ptr;


class Column : public QWidget {
    Q_OBJECT

 private:
    QVBoxLayout* layout;
    Navbar* navbar;
    QStackedLayout* stacked_layout;
    bool is_welcome_mode;

    vector<QString> titles;

    void notify_title_changed();


 protected:
    void mousePressEvent(QMouseEvent*);

 public:
    explicit Column(QWidget* parent = nullptr, bool is_welcome_mode = false);
    int count();

 signals:
    void title_changed(QString s);


 public slots:
    void open_user_feed(shared_ptr<User> u);
    void open_tweet(shared_ptr<Tweet> t);
    void open_timeline();
    void open_followed_users();
    void open_search_feed(SearchParams params);

    void go_back();
    void do_searchbar(QString s);
    void entity_clicked(QString target);
    void exit_welcome_mode_if_needed();

    void search_tweet(TweetID id);
    void search_user(UserHandle handle);
};

#endif  // COLUMN_WIDGET_H
