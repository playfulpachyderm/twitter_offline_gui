#include "missing_tweet_placeholder.h"

MissingTweetPlaceholder::MissingTweetPlaceholder(TweetID tweet_id, QWidget* parent): QWidget(parent) {
	this->tweet_id = tweet_id;

	layout = new QHBoxLayout(this);

	label = new QPushButton(this);
	label->setText("(!) Tweet is not available in database");
	layout->addWidget(label);

	connect(label, SIGNAL(clicked()), this, SLOT(load_tweet()));
}

void MissingTweetPlaceholder::load_tweet() {
    QCursor tmp = cursor();
    setCursor(Qt::WaitCursor);
    int result = Tweet::download_conversation(tweet_id);
    printf("Result: %d\n", result);  // TODO: if (result != 0) {...}
    setCursor(tmp);
}
