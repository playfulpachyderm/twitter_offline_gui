#include "timeline.h"

#include <Qt>
#include <QScrollBar>
#include <QSizePolicy>

#include "../models/feeds/feed.h"

#include "../lib/global_state.h"


Timeline::Timeline(Feed feed, Column* parent): QScrollArea(parent) {
    this->column = parent;

    setWidgetResizable(true);
    setFrameShape(QFrame::NoFrame);

    dummy = new QWidget(this);
    layout = new QVBoxLayout(dummy);
    layout->setSpacing(0);
    layout->setContentsMargins(0, 0, 0, 0);
    dummy->setLayout(layout);
    dummy->setFixedWidth(width() - verticalScrollBar()->width());
    dummy->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Maximum);

    if (feed.items.size() == 0) {
        // If there are no tweets yet: Show a warning label
        no_tweets_label = new QLabel(this);
        no_tweets_label->setText("Your timeline is empty!  Once you follow some people and download their tweets, they will show up here.\n\n");
        no_tweets_label->setWordWrap(true);
        no_tweets_label->setAlignment(Qt::AlignVCenter | Qt::AlignHCenter);

        QFont big_text_font = font();
        big_text_font.setPixelSize(35);
        no_tweets_label->setFont(big_text_font);
        no_tweets_label->setFixedHeight(big_text_font.pixelSize() * 8);

        QPalette pal = palette();
        pal.setColor(QPalette::WindowText, QColor(200, 200, 200));  // TODO: colors
        no_tweets_label->setPalette(pal);

        // Use an inner layout to create padding around the edges
        QVBoxLayout* inner_layout = new QVBoxLayout();
        inner_layout->setContentsMargins(60, 0, 60, 0);
        inner_layout->addWidget(no_tweets_label);
        layout->addLayout(inner_layout);
    } else {
        // If there are tweets, show them!
        tweet_feed = new TweetFeed(feed, this, column);
        layout->addWidget(tweet_feed);
        connect(tweet_feed, SIGNAL(load_more_tweets()), this, SLOT(load_more_tweets()));

        layout->addStretch(1);
    }

    setWidget(dummy);
}

void Timeline::resizeEvent(QResizeEvent* e) {
    if (e == nullptr) {
        printf("e is nullptr!\n");
    }
    dummy->setFixedWidth(width() - verticalScrollBar()->width());
    printf("Resizing User Feed from (%d, %d) to (%d, %d)\n", e->oldSize().width(), e->oldSize().height(), e->size().width(), e->size().height());
    if (no_tweets_label != nullptr) {
        no_tweets_label->setFixedHeight(e->size().height());
    }
    QScrollArea::resizeEvent(e);
}

void Timeline::download_new_tweets() {
    // TODO (Need to implement a new engine function for this!)
}

/**
 * Get more tweets and add them to the feed
 */
void Timeline::load_more_tweets() {
    Feed feed = Feed::get_timeline(GlobalState::USER_FEED_TWEET_BATCH_SIZE, tweet_feed->get_oldest_timestamp());
    tweet_feed->extend_with(feed);
}
