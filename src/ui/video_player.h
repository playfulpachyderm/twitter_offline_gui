#ifndef VIDEO_PLAYER_H
#define VIDEO_PLAYER_H

#include <QWidget>
#include <QMouseEvent>
#include <QMediaPlayer>
#include <QVideoWidget>
#include <QSlider>
#include <QVBoxLayout>
#include <QSize>
#include <QResizeEvent>

#include "../models/tweet.h"

class VideoPlayer : public QWidget {
    Q_OBJECT

 private:
    int max_height;
    int max_width;

    QVBoxLayout* layout;
    QMediaPlayer* media_player;
    QVideoWidget* video_widget;
    QSlider* progress_slider = nullptr;

 public slots:
    void restart_video(QMediaPlayer::State media_state);
    void set_media_position(int position);
    void set_progress_slider_value(qint64 value);
    void set_progress_slider_maximum(qint64 maximum);

 protected:
    void mousePressEvent(QMouseEvent* e);
    void resizeEvent(QResizeEvent* e);

 public:
    shared_ptr<Video> video;
    explicit VideoPlayer(QWidget* parent = nullptr, shared_ptr<Video> video = nullptr);
    QSize sizeHint() const;
};


#endif  // VIDEO_PLAYER_H
