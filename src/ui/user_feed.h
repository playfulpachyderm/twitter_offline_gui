#ifndef USER_FEED_H
#define USER_FEED_H

#include <QWidget>
#include <QScrollArea>
#include <QVBoxLayout>
#include <QResizeEvent>

#include <vector>
#include <memory>

#include "../models/user.h"

#include "./tweet_widget.h"
#include "./column.h"
#include "./user_feed_header.h"

#include "./components/tweet_feed.h"

using std::vector;
using std::shared_ptr;


class UserFeed : public QScrollArea {
    Q_OBJECT

 private:
    shared_ptr<User> u;

    Column* column;

    QWidget* dummy;
    QVBoxLayout* layout;
    UserFeedHeader* header;
    TweetFeed* tweet_feed;

 public slots:
    void download_new_tweets();
    void download_user_content();
    void resizeEvent(QResizeEvent* e);
    void load_more_tweets();


 public:
    explicit UserFeed(Column* parent = nullptr, shared_ptr<User> u = nullptr);
};

#endif  // USER_FEED_H
