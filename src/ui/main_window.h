#ifndef TWITTER_MAIN_WINDOW_H
#define TWITTER_MAIN_WINDOW_H

#include <QMainWindow>
#include <QMenuBar>
#include <QString>

#include "./column.h"


class MainWindow : public QMainWindow {
    Q_OBJECT

 private:
    Column* column;
    QMenuBar* menu_bar;
    QMenu* file_menu;
    QMenu* help_menu;

 private slots:
    void choose_profile_dir();
    void new_profile_dir();

    void display_about_page();
    void display_instructions_page();

 public slots:
    void update_title(QString s);

 public:
    explicit MainWindow();

    void set_profile_directory_to(QString);
};


#endif  // TWITTER_MAIN_WINDOW_H
