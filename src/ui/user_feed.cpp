#include "user_feed.h"

#include <Qt>
#include <QFrame>
#include <QCursor>
#include <QScrollBar>
#include <QSizePolicy>

#include <memory>

#include "../models/tweet.h"
#include "../models/feeds/feed.h"

#include "../lib/global_state.h"

using std::make_shared;
using std::shared_ptr;


UserFeed::UserFeed(Column* parent, shared_ptr<User> u): QScrollArea(parent) {
    this->u = u;
    this->column = parent;

    setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOn);
    setWidgetResizable(true);
    setFrameShape(QFrame::NoFrame);

    dummy = new QWidget(this);
    layout = new QVBoxLayout(dummy);
    layout->setSpacing(0);
    layout->setContentsMargins(0, 0, 0, 0);
    dummy->setLayout(layout);
    dummy->setFixedWidth(width() - verticalScrollBar()->width());
    dummy->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Maximum);

    header = new UserFeedHeader(dummy, u);
    layout->addWidget(header);

    Feed feed = Feed::get_user_feed(u, GlobalState::USER_FEED_TWEET_BATCH_SIZE, -1);

    tweet_feed = new TweetFeed(feed, this, column);
    layout->addWidget(tweet_feed);
    connect(tweet_feed, SIGNAL(load_more_tweets()), this, SLOT(load_more_tweets()));

    layout->addStretch(1);
    setWidget(dummy);
}

void UserFeed::resizeEvent(QResizeEvent* e) {
    dummy->setFixedWidth(width() - verticalScrollBar()->width());
    printf("Resizing User Feed from (%d, %d) to (%d, %d)\n", e->oldSize().width(), e->oldSize().height(), e->size().width(), e->size().height());
}

void UserFeed::download_new_tweets() {
    QCursor tmp = cursor();
    setCursor(Qt::WaitCursor);
    int result = u->download_new_tweets();
    printf("Result: %d\n", result);
    setCursor(tmp);
}

/**
 * Download this user's profile image and banner image, then refresh the header, with the new images
 */
void UserFeed::download_user_content() {
    QCursor tmp = cursor();
    setCursor(Qt::WaitCursor);
    int result = User::fetch_user(u->handle);
    printf("Result: %d\n", result);
    setCursor(tmp);

    GlobalState::delete_cached_profile_img(u);
    u->reload_from_db();

    // Refresh the header
    QLayoutItem* header_layout_item = layout->takeAt(0);
    delete header_layout_item->widget();
    delete header_layout_item;

    header = new UserFeedHeader(dummy, u);  // header == header_layout_item->widget()
    layout->insertWidget(0, header);
}

/**
 * Get more tweets and add them to the feed
 */
void UserFeed::load_more_tweets() {
    Feed feed = Feed::get_user_feed(u, GlobalState::USER_FEED_TWEET_BATCH_SIZE, tweet_feed->get_oldest_timestamp());
    tweet_feed->extend_with(feed);
}
