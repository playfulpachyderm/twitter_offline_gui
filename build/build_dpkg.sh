#!/bin/bash

set -e

if [[ -z "$1" ]]
then
	# Error message and exit
	>&2 echo "No version number provided!  Exiting."
	exit 1
fi

# Prepare the output folder
if [[ -e dpkg_tmp ]]
then
	rm -rf dpkg_tmp
fi
mkdir dpkg_tmp

# Construct the dpkg directory structure
mkdir -p dpkg_tmp/usr/local/bin
cp ../src/gen/Twitter dpkg_tmp/usr/local/bin/Twitter


# Create the Desktop files
mkdir -p dpkg_tmp/usr/share/pixmaps
cp ../resources/icons/twitter.png dpkg_tmp/usr/share/pixmaps

mkdir -p dpkg_tmp/usr/share/applications
echo "[Desktop Entry]
Name=Offline Twitter
Comment=Desktop client for browsing and archiving Twitter
Exec=/usr/local/bin/Twitter
Icon=/usr/share/pixmaps/twitter.png
Terminal=false
Type=Application
Categories=Application;Qt;Network
StartupNotify=true
" > dpkg_tmp/usr/share/applications/Twitter.desktop


# Create the `DEBIAN/control` file
mkdir dpkg_tmp/DEBIAN
echo "Package: offline-twitter
Version: $1
Architecture: all
Maintainer: me@playfulpachyderm.com
Installed-Size: 930
Depends: offline-twitter-engine (>= 0.1.3), libqt5widgets5, libqt5multimediawidgets5, libqt5sql5, libqt5opengl5, libgcrypt20
Section: web
Priority: optional
Homepage: http://offline-twitter.com
Description: Qt-based desktop app for displaying backed-up tweet content.
 When browsing, a local copy of everything you view is automatically saved
 to a SQLite database.  You can view this content without being online.
" > dpkg_tmp/DEBIAN/control


dpkg-deb --build `pwd`/dpkg_tmp .
