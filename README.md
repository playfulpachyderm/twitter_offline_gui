# Offline Twitter

Offline Twitter is an alternative client for browsing Twitter, which automatically saves a local copy of everything you
view.  It is a native desktop app built using Qt5.

## Why Offline Twitter?

I wanted to create a way to save twitter content without any dependencies, so I could view it without having to go
through a web interface using a browser.  There are several reasons why I wanted to do this.  Here are some of them:

1. The twitter web client is garbage, and gets worse every month:
	- it uses a ton of memory
	- it's slow
	- it's full of spyware and ads
	- there's always annoying left-wing political nonsense in the sidebar
	- recently there seem to be a number of layout-related bugs

1. There's this weird "algorithm" that nobody knows what it is, but for some reason it prevents you from seeing tweets
from people you're subscribed to.

1. Twitter is becoming increasingly censorious.  A lot of great content is being lost as Twitter bans people all over
the place.  I wanted to create a way to save a personal copy of everything on Twitter that I liked, without worrying
about it disappearing.

This application can be used simply as an alternative client for browsing twitter, without all those problems.  You can
read more about why you should oppose web browsers on general principles [here](https://drewdevault.com/2020/03/18/Reckless-limitless-scope.html).

## What does Offline Twitter do?

Offline Twitter lets you browse Twitter without worrying about those problems.  As you browse, it saves everything you
view as a local copy on your disk.  It's yours until you decide to delete it.  There is no spyware.  There are no ads.

Offline Twitter offers a smooth, clean browsing experience that doesn't try to annoy you.  The data is stored on your
computer in a simple and easy-to-understand format, so you can view it any way you like.  For example, you can view
photos and videos using your desktop environment's default media viewers (e.g., VLC Player, Windows Photo Gallery, or
whatever you like to use), because they're just saved as regular files onto your computer.

## How does Offline Twitter work?

Offline Twitter is actually two different applications: the engine, which scrapes tweets from the Twitter api, and the
GUI, which displays them in a way to look visually similar to the Twitter web UI.  The GUI requires the engine in order
to work properly, but if you want to use a different viewer, or make your own, you could just use the engine and
combine it with a different front-end.

The engine is written in Go; it's a binary called `twitter` (lowercase).  The GUI is written in C++ using Qt5, and is
a binary called `Twitter` (uppercase).

The engine can be found at: https://gitlab.com/playfulpachyderm/twitter_offline_engine

The data storage format is a SQLite database for text content, and regular files for media (images, videos, and gifs).
The data is stored in a single directory (I currently call it a "profile" in the app, but I'm not entirely happy with
this name).  This profile directory contains:
- a `twitter.db` file (the SQLite database)
- a couple of folders like `profile_images`, `videos`, which are descriptively named
- a `settings.yaml` file, which contains user settings and configurations
- a `users.yaml` file, which represents a list of Twitter users that you want to "follow"

A profile directory represents an entire collection of tweets.  If you use Twitter for multiple purposes (e.g.,
politics, health advice, memes, knitting ideas, etc.), you can create a different profile directory for each one.  This
is for if, for example, you don't want posts about cute designs for a scarf to be intermixed with angry political
arguments.  If you don't care about this, then just use one profile directory for everything!

You can switch between profile directories in the app.

## Installing

This application is under rapid development.  It's currently available in alpha release as an `apt` package.  It doesn't
work very well yet.

Install using apt:

```sh
# Make sure you have `curl` and `gnupg` installed
apt install -y curl gnupg

# Add my GPG key
curl https://apt.playfulpachyderm.com/KEY.gpg | sudo apt-key add -

# Add my apt repo as an apt source
echo "deb https://apt.playfulpachyderm.com/ ./" | sudo tee /etc/apt/sources.list.d/offline-twitter.list
apt update

# Install offline-twitter
apt install offline-twitter
```

This should install both binaries, `twitter` and `Twitter`:
```sh
$ which twitter
/usr/local/bin/twitter
$ which Twitter
/usr/local/bin/Twitter
```

If you want Offline Twitter available on another platform, assistance is very welcome!  I want to make it available on
Windows and Mac soon, but I don't have a Windows or a Mac machine.

## Building from source

Install Qt5 for your platform.  On Ubuntu, this was `apt install qt5-default`.  It is probably different (and possibly
harder) on other platforms.

You can look at the CI build file (`.build.yml`) to see how it builds in a Ubuntu container.  The summary is here:

```sh
git clone https://gitlab.com/playfulpachyderm/twitter_offline_gui.git
cd twitter_offline_gui/src
mkdir gen
qmake -o gen Twitter.qmake
cd gen
make -j$(nproc)

./Twitter
```
