#include <memory>

#include "include_catch2/catch.h"

#include "../src/models/tweet.h"
#include "../src/models/_db.h"

#define private public
#include "../src/ui/interactions_bar.h"

#define TEST_PROFILE_DIR "../../sample_data/profile"

using std::make_shared;


TEST_CASE("Should make an InteractionsBar widget") {
	set_profile_directory(TEST_PROFILE_DIR);
	Tweet t = Tweet::get_tweet_by_id(1413664406995566593);

	InteractionsBar bar(nullptr, make_shared<Tweet>(t));

	REQUIRE(bar.likes_count_label->text() == "440");
	REQUIRE(bar.retweets_count_label->text() == "68");
	REQUIRE(bar.replies_count_label->text() == "9");
	REQUIRE(bar.quote_tweets_count_label->text() == "5");
}
