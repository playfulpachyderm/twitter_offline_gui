#include <QSignalSpy>

#include "include_catch2/catch.h"

#include "../src/models/_db.h"

#define private public

#include "../src/models/tweet.h"
#include "../src/models/user.h"
#include "../src/lib/global_state.h"

#define protected public

#include "../src/ui/column.h"
#include "../src/ui/user_feed.h"
#include "../src/ui/conversation.h"
#include "../src/ui/timeline.h"
#include "../src/ui/search_feed.h"
#include "../src/ui/followed_list.h"

#include "./ui_test_utils.h"

#define TEST_PROFILE_DIR "../../sample_data/profile"

using std::make_shared;


TEST_CASE("Should make a Column") {
	set_profile_directory(TEST_PROFILE_DIR);

	Column c(nullptr);
	REQUIRE(c.stacked_layout->count() == 0);

	QSignalSpy spy(&c, SIGNAL(title_changed(QString)));

	User u = User::get_user_by_handle("CovfefeAnon");
	REQUIRE(spy.count() == 0);
	c.open_user_feed(make_shared<User>(u));
	REQUIRE(c.stacked_layout->count() == 1);
	REQUIRE(c.stacked_layout->currentIndex() == 0);
	REQUIRE(spy.count() == 1);
	REQUIRE(spy.takeFirst().at(0).toString() == "@CovfefeAnon");
	REQUIRE(c.navbar->depth_label->text() == "1");
	REQUIRE(static_cast<UserFeed*>(c.stacked_layout->itemAt(0)->widget())->u->id == u.id);

	User u2 = User::get_user_by_handle("kwamurai");
	REQUIRE(spy.count() == 0);
	c.open_user_feed(make_shared<User>(u2));
	REQUIRE(c.stacked_layout->count() == 2);
	REQUIRE(c.stacked_layout->currentIndex() == 1);
	REQUIRE(spy.count() == 1);
	REQUIRE(spy.takeFirst().at(0).toString() == "@kwamurai");
	REQUIRE(c.navbar->depth_label->text() == "2");
	REQUIRE(static_cast<UserFeed*>(c.stacked_layout->itemAt(1)->widget())->u->id == u2.id);

	Tweet t = Tweet::get_tweet_by_id(1413773185296650241);
	REQUIRE(spy.count() == 0);
	c.open_tweet(make_shared<Tweet>(t));
	REQUIRE(c.stacked_layout->count() == 3);
	REQUIRE(c.stacked_layout->currentIndex() == 2);
	REQUIRE(spy.count() == 1);
	REQUIRE(spy.takeFirst().at(0).toString() == "@Peter_Nimitz: \"Good idea in theory, but in practice mostly graft\"");
	REQUIRE(c.navbar->depth_label->text() == "3");
	REQUIRE(static_cast<Conversation*>(c.stacked_layout->itemAt(2)->widget())->t->id == t.id);

	REQUIRE(spy.count() == 0);
	c.go_back();
	REQUIRE(c.stacked_layout->count() == 2);
	REQUIRE(c.stacked_layout->currentIndex() == 1);
	REQUIRE(spy.count() == 1);
	REQUIRE(spy.takeFirst().at(0).toString() == "@kwamurai");
	REQUIRE(c.navbar->depth_label->text() == "2");
	REQUIRE(static_cast<UserFeed*>(c.stacked_layout->itemAt(1)->widget())->u->id == u2.id);

	REQUIRE(spy.count() == 0);
	c.go_back();
	REQUIRE(c.stacked_layout->count() == 1);
	REQUIRE(c.stacked_layout->currentIndex() == 0);
	REQUIRE(spy.count() == 1);
	REQUIRE(spy.takeFirst().at(0).toString() == "@CovfefeAnon");
	REQUIRE(c.navbar->depth_label->text() == "1");
	REQUIRE(static_cast<UserFeed*>(c.stacked_layout->itemAt(0)->widget())->u->id == u.id);

	REQUIRE(spy.count() == 0);
	c.go_back();
	REQUIRE(c.stacked_layout->count() == 0);
	REQUIRE(c.navbar->depth_label->text() == "0");
	REQUIRE(spy.count() == 1);
	REQUIRE(spy.takeFirst().at(0).toString() == "No views active");
}

TEST_CASE("Shouldn't be able to use `go_back` if the navigation stack is empty") {
	set_profile_directory(TEST_PROFILE_DIR);

	Column c(nullptr);
	REQUIRE(c.stacked_layout->count() == 0);

	c.go_back();
	REQUIRE(c.stacked_layout->count() == 0);  // Make sure it didn't change
}


// Test the search bar
// -------------------

TEST_CASE("Should open a tweet by full URL") {
	set_profile_directory(TEST_PROFILE_DIR);

	Column c(nullptr);
	REQUIRE(c.stacked_layout->count() == 0);

	c.do_searchbar("https://twitter.com/Peter_Nimitz/status/1413773185296650241");
	REQUIRE(c.stacked_layout->count() == 1);
	REQUIRE(static_cast<Conversation*>(c.stacked_layout->itemAt(0)->widget())->t->id == 1413773185296650241);
}

TEST_CASE("Should open a tweet by TweetID") {
	set_profile_directory(TEST_PROFILE_DIR);

	Column c(nullptr);
	REQUIRE(c.stacked_layout->count() == 0);

	c.do_searchbar("1413773185296650241");
	REQUIRE(c.stacked_layout->count() == 1);
	REQUIRE(static_cast<Conversation*>(c.stacked_layout->itemAt(0)->widget())->t->id == 1413773185296650241);
}

TEST_CASE("Should not crash if a tweet isn't found") {
	set_profile_directory(TEST_PROFILE_DIR);

	Column c(nullptr);
	REQUIRE(c.stacked_layout->count() == 0);

	GlobalState::TWITTER_ENGINE.fake_return_code = 1;  // Simulate failure
	c.do_searchbar("3");  // Nonexistent tweet
	REQUIRE(c.stacked_layout->count() == 0);
}

TEST_CASE("Should use the search function to find a user by handle") {
	set_profile_directory(TEST_PROFILE_DIR);

	Column c(nullptr);
	REQUIRE(c.stacked_layout->count() == 0);

	c.do_searchbar("@CovfefeAnon");
	REQUIRE(c.stacked_layout->count() == 1);
	REQUIRE(static_cast<UserFeed*>(c.stacked_layout->itemAt(0)->widget())->u->handle == "CovfefeAnon");
}

TEST_CASE("Should warn if user handle has a space in it") {
	set_profile_directory(TEST_PROFILE_DIR);

	Column c(nullptr);
	REQUIRE(c.stacked_layout->count() == 0);

	c.do_searchbar("@general ben");
	REQUIRE(c.stacked_layout->count() == 0);
}

TEST_CASE("Should search for something") {
	set_profile_directory(TEST_PROFILE_DIR);

	Column c(nullptr);
	REQUIRE(c.stacked_layout->count() == 0);

	c.do_searchbar("think but");
	REQUIRE(c.stacked_layout->count() == 1);
	REQUIRE(static_cast<SearchFeed*>(c.stacked_layout->itemAt(0)->widget())->cursor->search_params.get_original_query() == "think but");
}

TEST_CASE("Searching with imbalanced quotes should do nothing") {
	set_profile_directory(TEST_PROFILE_DIR);

	Column c(nullptr);
	REQUIRE(c.stacked_layout->count() == 0);

	c.do_searchbar("think \"but");
	REQUIRE(c.stacked_layout->count() == 0);
}

TEST_CASE("Searching for nothing (empty string) should do nothing") {
	set_profile_directory(TEST_PROFILE_DIR);

	Column c(nullptr);
	REQUIRE(c.stacked_layout->count() == 0);

	c.do_searchbar("");
	REQUIRE(c.stacked_layout->count() == 0);
}


// Test clicking on stuff
// ----------------------

TEST_CASE("Clicking on a user profile should open that user") {
	set_profile_directory(TEST_PROFILE_DIR);

	Column c(nullptr);
	REQUIRE(c.stacked_layout->count() == 0);
	Tweet t = Tweet::get_tweet_by_id(1413773185296650241);
	c.open_tweet(make_shared<Tweet>(t));

	REQUIRE(c.stacked_layout->count() == 1);

	QMouseEvent click_event = get_left_click();
	static_cast<Conversation*>(c.stacked_layout->itemAt(0)->widget())->main_tweet->author_info_panel->mousePressEvent(&click_event);
	REQUIRE(c.stacked_layout->count() == 2);
	REQUIRE(static_cast<UserFeed*>(c.stacked_layout->itemAt(1)->widget())->u->handle == "Peter_Nimitz");
}

TEST_CASE("Clicking on a tweet should open that tweet") {
	set_profile_directory(TEST_PROFILE_DIR);

	Column c(nullptr);
	REQUIRE(c.stacked_layout->count() == 0);

	User u = User::get_user_by_handle("Peter_Nimitz");
	c.open_user_feed(make_shared<User>(u));

	REQUIRE(c.stacked_layout->count() == 1);

	UserFeed* the_feed = static_cast<UserFeed*>(c.stacked_layout->itemAt(0)->widget());
	REQUIRE(the_feed->tweet_feed->tweet_widgets.size() == 6);
	QMouseEvent click_event = get_left_click();
	the_feed->tweet_feed->tweet_widgets[0]->mousePressEvent(&click_event);

	REQUIRE(c.stacked_layout->count() == 2);
	REQUIRE(static_cast<Conversation*>(c.stacked_layout->itemAt(1)->widget())->t->id == 1413773185296650241);
}

TEST_CASE("Clicking the currently open tweet should do nothing") {
	set_profile_directory(TEST_PROFILE_DIR);

	Column c(nullptr);
	REQUIRE(c.stacked_layout->count() == 0);

	Tweet t = Tweet::get_tweet_by_id(1413773185296650241);
	c.open_tweet(make_shared<Tweet>(t));

	REQUIRE(c.stacked_layout->count() == 1);

	Conversation* the_conversation = static_cast<Conversation*>(c.stacked_layout->itemAt(0)->widget());
	QMouseEvent click_event = get_left_click();
	the_conversation->main_tweet->mousePressEvent(&click_event);  // Should do nothing!
	REQUIRE(c.stacked_layout->count() == 1);
}

TEST_CASE("Back-clicking should go back") {
	set_profile_directory(TEST_PROFILE_DIR);

	Column c(nullptr);
	REQUIRE(c.stacked_layout->count() == 0);
	Tweet t = Tweet::get_tweet_by_id(1413773185296650241);
	c.open_tweet(make_shared<Tweet>(t));

	REQUIRE(c.stacked_layout->count() == 1);

	QMouseEvent click_event = get_back_click();
	c.mousePressEvent(&click_event);

	REQUIRE(c.stacked_layout->count() == 0);
}

TEST_CASE("Welcome Mode should work") {
	Column c(nullptr, true);  // Enable "is_welcome_mode"
	REQUIRE(c.is_welcome_mode == true);

	// There should be a Welcome Screen
	REQUIRE(c.stacked_layout->count() == 1);

	// The Welcome Screen doesn't count toward the "count"
	REQUIRE(c.count() == 0);

	// Navbar should be disabled
	REQUIRE(c.navbar->isEnabled() == false);

	// Test exiting the welcome mode
	c.exit_welcome_mode_if_needed();
	REQUIRE(c.is_welcome_mode == false);
	REQUIRE(c.count() == 0);
	REQUIRE(c.stacked_layout->count() == 0);
	REQUIRE(c.navbar->isEnabled() == true);
}


// Test clicking entities
// ----------------------

/**
 * Test clicking an @-mention in a tweet; it should open the @'d user
 */
TEST_CASE("Clicking @-mentions in tweet body") {
	set_profile_directory(TEST_PROFILE_DIR);

	Column c(nullptr);
	Tweet t = Tweet::get_tweet_by_id(1413773185296650241);
	t.text = "@michaelmalice";

	TweetWidget widget(nullptr, make_shared<Tweet>(t), &c);

	REQUIRE(c.stacked_layout->count() == 0);

	// Clicking on the text label should open the @user in the column.
	// The co-ords of the click are (10, 10), hopefully this is a decent spot
	QMouseEvent click_event = get_left_click();
	widget.text_label->mousePressEvent(&click_event);

	// Should have opened the user in the column
	REQUIRE(c.stacked_layout->count() == 1);
	REQUIRE(c.stacked_layout->itemAt(0) != nullptr);
	REQUIRE(c.stacked_layout->itemAt(0)->widget() != nullptr);
	REQUIRE(static_cast<UserFeed*>(c.stacked_layout->itemAt(0)->widget())->u->handle == "michaelmalice");
}

/**
 * Clicking should also work in a user's bio
 */
TEST_CASE("Clicking @-mentions in a user feed header") {
	set_profile_directory(TEST_PROFILE_DIR);

	Column c(nullptr);
	shared_ptr<User> u = make_shared<User>(User::get_user_by_handle("kwamurai"));
	u->bio = "@michaelmalice";

	UserFeed feed(&c, u);

	REQUIRE(c.stacked_layout->count() == 0);

	QMouseEvent click_event = get_left_click();
	feed.header->bio_label->mousePressEvent(&click_event);

	// Should have opened the user in the column
	REQUIRE(c.stacked_layout->count() == 1);
	REQUIRE(c.stacked_layout->itemAt(0) != nullptr);
	REQUIRE(c.stacked_layout->itemAt(0)->widget() != nullptr);
	REQUIRE(static_cast<UserFeed*>(c.stacked_layout->itemAt(0)->widget())->u->handle == "michaelmalice");

}

// Test opening  misc views
// ------------------------

TEST_CASE("Should open the timeline") {
	set_profile_directory(TEST_PROFILE_DIR);

	Column c(nullptr);
	REQUIRE(c.stacked_layout->count() == 0);

	c.open_timeline();
	REQUIRE(c.stacked_layout->count() == 1);
	REQUIRE(static_cast<Timeline*>(c.stacked_layout->itemAt(0)->widget())->tweet_feed->tweet_widgets.size() > 0);
}

TEST_CASE("Should open the Followed Users list") {
	set_profile_directory(TEST_PROFILE_DIR);

	Column c(nullptr);
	REQUIRE(c.stacked_layout->count() == 0);

	c.open_followed_users();
	REQUIRE(c.stacked_layout->count() == 1);
	REQUIRE(static_cast<FollowedList*>(c.stacked_layout->itemAt(0)->widget())->layout->count() > 0);
}
