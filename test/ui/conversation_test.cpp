#include "include_catch2/catch.h"

#include "../src/models/tweet.h"
#include "../src/models/_db.h"

#define private public

#include "../src/ui/conversation.h"
#include "../src/ui/tweet_widget.h"
#include "../src/ui/missing_tweet_placeholder.h"

#define TEST_PROFILE_DIR "../../sample_data/profile"

using std::make_shared;


TEST_CASE("Should make a Conversation") {
	set_profile_directory(TEST_PROFILE_DIR);
	Tweet t = Tweet::get_tweet_by_id(1413773185296650241);

	Conversation c(nullptr, make_shared<Tweet>(t));

	REQUIRE(c.layout->count() == 5);  // Stretch element
	REQUIRE(static_cast<TweetWidget*>(c.layout->itemAt(0)->widget())->t->id == 1413646309047767042);
	REQUIRE(static_cast<TweetWidget*>(c.layout->itemAt(1)->widget())->t->id == 1413646595493568516);
	REQUIRE(static_cast<TweetWidget*>(c.layout->itemAt(2)->widget())->t->id == 1413772782358433792);
	REQUIRE(static_cast<TweetWidget*>(c.layout->itemAt(3)->widget())->t->id == 1413773185296650241);

	// Stretch element at end
	REQUIRE(c.layout->itemAt(4)->widget() == nullptr);
	REQUIRE(c.layout->itemAt(4)->spacerItem() != nullptr);
}

TEST_CASE("Should render replies 2 layers deep") {
	set_profile_directory(TEST_PROFILE_DIR);
	Tweet t = Tweet::get_tweet_by_id(1413657324267311104);

	Conversation c(nullptr, make_shared<Tweet>(t));

	REQUIRE(c.layout->count() == 6);  // Stretch element
	REQUIRE(static_cast<TweetWidget*>(c.layout->itemAt(0)->widget())->t->id == 1413646309047767042);
	REQUIRE(static_cast<TweetWidget*>(c.layout->itemAt(1)->widget())->t->id == 1413646595493568516);
	REQUIRE(static_cast<TweetWidget*>(c.layout->itemAt(2)->widget())->t->id == 1413657324267311104);

	// The replies
	REQUIRE(static_cast<TweetWidget*>(c.layout->itemAt(3)->widget())->t->id == 1413658466795737091);
	REQUIRE(static_cast<TweetWidget*>(c.layout->itemAt(4)->widget())->t->id == 1413666994876936198);
}

TEST_CASE("Should render full thread") {
	set_profile_directory(TEST_PROFILE_DIR);
	Tweet t = Tweet::get_tweet_by_id(1413646309047767042);

	Conversation c(nullptr, make_shared<Tweet>(t));

	REQUIRE(static_cast<TweetWidget*>(c.layout->itemAt(0)->widget())->t->id == 1413646309047767042);  // Original tweet
	REQUIRE(static_cast<TweetWidget*>(c.layout->itemAt(1)->widget())->t->id == 1413646595493568516);  // First reply
	REQUIRE(static_cast<TweetWidget*>(c.layout->itemAt(2)->widget())->t->id == 1413647919215906817);  // Second reply
}

TEST_CASE("Should not crash when a parent tweet in the thread is missing") {
	set_profile_directory(TEST_PROFILE_DIR);
	Tweet t = Tweet::get_tweet_by_id(1513313535480287235);

	// Check that the in_reply_to tweet is missing
	try {
		t.get_replied_tweet();
		REQUIRE(false);  // Should raise DBException because parent tweet doesn't exist
	} catch (DBException &e) {}

	Conversation c(nullptr, make_shared<Tweet>(t));
	REQUIRE(static_cast<TweetWidget*>(c.layout->itemAt(1)->widget())->t->id == 1513313535480287235);  // Main tweet
	REQUIRE(static_cast<MissingTweetPlaceholder*>(c.layout->itemAt(0)->widget())->tweet_id == t.in_reply_to_id);
}
