#include "include_catch2/catch.h"

#include "../src/models/_db.h"
#include "../src/models/feeds/feed.h"

#define private public

#include "../src/lib/global_state.h"

#include "../src/ui/timeline.h"
#include "../src/ui/tweet_widget.h"

#define TEST_PROFILE_DIR "../../sample_data/profile"


TEST_CASE("Should make an empty Timeline") {
	set_profile_directory(TEST_PROFILE_DIR);

	// Use "max posted at" = 1 to produce an empty list
	Feed f = Feed::get_timeline(GlobalState::USER_FEED_TWEET_BATCH_SIZE, 1);
	REQUIRE(f.items.size() == 0);  // Must be empty;

	Timeline timeline(f, nullptr);
	REQUIRE(timeline.no_tweets_label != nullptr);
}

TEST_CASE("Should make a Timeline") {
	set_profile_directory(TEST_PROFILE_DIR);
	GlobalState::USER_FEED_TWEET_BATCH_SIZE = 5;

	Feed f = Feed::get_timeline(GlobalState::USER_FEED_TWEET_BATCH_SIZE, -1);
	Timeline timeline(f, nullptr);

	REQUIRE(timeline.layout->count() == 2);  // 0) tweets; 1) stretch element
	REQUIRE(timeline.no_tweets_label == nullptr);
	REQUIRE(timeline.tweet_feed->tweets_layout->count() == f.items.size());

	// Check last tweet
	REQUIRE(timeline.tweet_feed->tweets_layout->itemAt(4)->widget() != nullptr);  // Last tweet
	REQUIRE(static_cast<TweetWidget*>(timeline.tweet_feed->tweets_layout->itemAt(4)->widget())->t->id == 1453461248142495744);  // Last tweet

	REQUIRE(timeline.tweet_feed->oldest_timestamp == 1635367140);
}

/**
 * Test pagination of a Timeline.  Should load more tweets on each `load_more_tweets` call,
 * until the end of feed is reached, then disable the `load_more_tweets` button.
 */
TEST_CASE("Should paginate a Timeline") {
	set_profile_directory(TEST_PROFILE_DIR);
	GlobalState::USER_FEED_TWEET_BATCH_SIZE = 4;

	Feed f = Feed::get_timeline(1000000, -1);  // Get all the tweets
	REQUIRE(f.items.size() == 18);

	Feed feed = Feed::get_timeline(GlobalState::USER_FEED_TWEET_BATCH_SIZE, -1);
	Timeline timeline(feed, nullptr);
	REQUIRE(timeline.tweet_feed->tweets_layout->count() == 4);

	timeline.load_more_tweets();
	REQUIRE(timeline.tweet_feed->tweets_layout->count() == 8);
	timeline.load_more_tweets();
	REQUIRE(timeline.tweet_feed->tweets_layout->count() == 12);
	timeline.load_more_tweets();
	REQUIRE(timeline.tweet_feed->tweets_layout->count() == 16);
	timeline.load_more_tweets();
	REQUIRE(timeline.tweet_feed->tweets_layout->count() == 18);

	// Try to load past end of timeline should disable the button
	REQUIRE(timeline.tweet_feed->load_more_tweets_button->isEnabled() == true);
	timeline.load_more_tweets();
	REQUIRE(timeline.tweet_feed->tweets_layout->count() == 18);
	REQUIRE(timeline.tweet_feed->load_more_tweets_button->isEnabled() == false);

	// Reset Global State
	GlobalState::USER_FEED_TWEET_BATCH_SIZE = 50;
}
