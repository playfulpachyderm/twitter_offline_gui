#ifndef UI_TEST_UTILS_H
#define UI_TEST_UTILS_H

#include <QMouseEvent>

QMouseEvent get_left_click();
QMouseEvent get_right_click();
QMouseEvent get_back_click();

#endif  // UI_TEST_UTILS_H
