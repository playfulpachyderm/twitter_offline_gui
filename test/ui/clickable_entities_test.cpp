#include "include_catch2/catch.h"

#include "../src/ui/utils/clickable_entities.h"

/**
 * It does not appear that CSS selectors work with Qt Style Sheets, so all styles unfortunately
 * have to be inline :V
 */
TEST_CASE("Should format @s and #s as rich text") {
	QString s1 = "Exit Group: Meet @extradeadjcb and hear about his alternative to #Woke Nation in Episode 46 of #ColemanNation with @RonColeman\nhttps://t.co/XwP22J3JrR";
	QString e1 = "Exit Group: Meet <a href='@extradeadjcb' style='text-decoration: none; color: rgb(27,149,224)'>@extradeadjcb</a> and hear about his alternative to <a href='#Woke' style='text-decoration: none; color: rgb(27,149,224)'>#Woke</a> Nation in Episode 46 of <a href='#ColemanNation' style='text-decoration: none; color: rgb(27,149,224)'>#ColemanNation</a> with <a href='@RonColeman' style='text-decoration: none; color: rgb(27,149,224)'>@RonColeman</a><br />https://t.co/XwP22J3JrR";
	REQUIRE(get_rich_text_for(s1).toStdString() == e1.toStdString());
	REQUIRE(s1 != e1);  // Should not modify the given string (idk how to use `const`)
}

TEST_CASE("Should escape html chars") {
	QString s1 = "Versus the real conversations we should be having has <20 likes despite how hard I have been pushing it";
	QString e1 = "Versus the real conversations we should be having has &lt;20 likes despite how hard I have been pushing it";
	REQUIRE(get_rich_text_for(s1).toStdString() == e1.toStdString());
}
