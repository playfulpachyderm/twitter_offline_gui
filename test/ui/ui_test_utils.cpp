#include <QPointF>
#include <Qt>
#include <QEvent>

#include "ui_test_utils.h"

QMouseEvent get_left_click() {
    return QMouseEvent(QEvent::MouseButtonRelease, QPointF(10,10), Qt::LeftButton, Qt::LeftButton, Qt::NoModifier);
}

QMouseEvent get_right_click() {
    return QMouseEvent(QEvent::MouseButtonRelease, QPointF(1,1), Qt::RightButton, Qt::RightButton, Qt::NoModifier);
}

QMouseEvent get_back_click() {
    return QMouseEvent(QEvent::MouseButtonRelease, QPointF(1,1), Qt::BackButton, Qt::BackButton, Qt::NoModifier);
}
