#define CATCH_CONFIG_FAST_COMPILE
#define CATCH_CONFIG_RUNNER


#include <QApplication>

#include <vector>

#include "include_catch2/catch.h"

#define private public
#include "../src/lib/global_state.h"
#undef private

using std::vector;


int main(int argc, char* argv[]) {
	GlobalState::TWITTER_ENGINE = Engine(true);  // Disable all engine calls

	// Add "-platform minimal" to argv
	vector<char*> new_argv(argv, argv + argc);
	new_argv.push_back(const_cast<char*>("-platform"));
	new_argv.push_back(const_cast<char*>("minimal"));
	new_argv.push_back(nullptr);

	int new_argc = argc + 2;

	QApplication app(new_argc, new_argv.data());

	int result = Catch::Session().run(argc, argv);
	return result;
}
