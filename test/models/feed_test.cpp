#include "include_catch2/catch.h"

#include "../src/models/_db.h"
#define private public
#include "../src/models/tweet.h"
#include "../src/models/user.h"
#include "../src/models/retweet.h"
#include "../src/models/feeds/feed.h"

#define TEST_PROFILE_DIR "../../sample_data/profile"


Tweet make_tweet(int posted_at) {
    Tweet ret;
    ret.posted_at = posted_at;
    return ret;
}
Retweet make_retweet(int retweeted_at) {
    Retweet ret;
    ret.retweeted_at = retweeted_at;
    return ret;
}

TEST_CASE("Should assemble a feed") {
    vector<Tweet> tweets;
    vector<Retweet> retweets;

    // Newest items at the front
    tweets.push_back(make_tweet(30));
    tweets.push_back(make_tweet(20));
    tweets.push_back(make_tweet(10));

    retweets.push_back(make_retweet(29));
    retweets.push_back(make_retweet(28));
    retweets.push_back(make_retweet(3));

    Feed feed = Feed::construct_from(tweets, retweets);

    REQUIRE(feed.items[0].tweet != nullptr);
    REQUIRE(feed.items[0].tweet->posted_at == 30);
    REQUIRE(feed.items[1].retweet != nullptr);
    REQUIRE(feed.items[1].retweet->retweeted_at == 29);
    REQUIRE(feed.items[2].retweet != nullptr);
    REQUIRE(feed.items[2].retweet->retweeted_at == 28);
    REQUIRE(feed.items[3].tweet != nullptr);
    REQUIRE(feed.items[3].tweet->posted_at == 20);
    REQUIRE(feed.items[4].tweet != nullptr);
    REQUIRE(feed.items[4].tweet->posted_at == 10);
    REQUIRE(feed.items[5].retweet != nullptr);
    REQUIRE(feed.items[5].retweet->retweeted_at == 3);

    REQUIRE(feed.get_oldest_timestamp() == 3);
}

TEST_CASE("Should assemble a feed with no retweets") {
    vector<Tweet> tweets;
    vector<Retweet> retweets;

    // Newest items at the front
    tweets.push_back(make_tweet(30));
    tweets.push_back(make_tweet(20));
    tweets.push_back(make_tweet(10));
    // No retweets


    Feed feed = Feed::construct_from(tweets, retweets);

    REQUIRE(feed.items[0].tweet != nullptr);
    REQUIRE(feed.items[0].tweet->posted_at == 30);
    REQUIRE(feed.items[1].tweet != nullptr);
    REQUIRE(feed.items[1].tweet->posted_at == 20);
    REQUIRE(feed.items[2].tweet != nullptr);
    REQUIRE(feed.items[2].tweet->posted_at == 10);
}

TEST_CASE("Should assemble a feed with no tweets") {
    vector<Tweet> tweets;
    vector<Retweet> retweets;

    // Newest items at the front
    // No tweets
    retweets.push_back(make_retweet(29));
    retweets.push_back(make_retweet(28));
    retweets.push_back(make_retweet(3));

    Feed feed = Feed::construct_from(tweets, retweets);

    REQUIRE(feed.items[0].retweet != nullptr);
    REQUIRE(feed.items[0].retweet->retweeted_at == 29);
    REQUIRE(feed.items[1].retweet != nullptr);
    REQUIRE(feed.items[1].retweet->retweeted_at == 28);
    REQUIRE(feed.items[2].retweet != nullptr);
    REQUIRE(feed.items[2].retweet->retweeted_at == 3);
}

TEST_CASE("Should get the beginning of a user's feed") {
    set_profile_directory(TEST_PROFILE_DIR);

    User user = User::get_user_by_handle("Cernovich");

    Feed first_2_tweets = Feed::get_user_feed(make_shared<User>(user), 2, -1);
    REQUIRE(first_2_tweets.items.size() == 2);

    REQUIRE(first_2_tweets.items[0].tweet == nullptr);
    REQUIRE(first_2_tweets.items[0].retweet != nullptr);
    REQUIRE(first_2_tweets.items[0].retweet->id == 1490135787144237058);
    REQUIRE(first_2_tweets.items[0].retweet->tweet_id == 1490120332484972549);

    REQUIRE(first_2_tweets.items[1].tweet == nullptr);
    REQUIRE(first_2_tweets.items[1].retweet != nullptr);
    REQUIRE(first_2_tweets.items[1].retweet->id == 1490119308692766723);
    REQUIRE(first_2_tweets.items[1].retweet->tweet_id == 1490116725395927042);

    REQUIRE(first_2_tweets.get_oldest_timestamp() == 1644107102);
}

TEST_CASE("Should get a user's feed in the middle") {
    set_profile_directory(TEST_PROFILE_DIR);

    User user = User::get_user_by_handle("Cernovich");

    Feed next_2_tweets = Feed::get_user_feed(make_shared<User>(user), 2, 1644107102);
    REQUIRE(next_2_tweets.items.size() == 2);

    REQUIRE(next_2_tweets.items[0].tweet == nullptr);
    REQUIRE(next_2_tweets.items[0].retweet != nullptr);
    REQUIRE(next_2_tweets.items[0].retweet->id == 1490100255987171332);
    REQUIRE(next_2_tweets.items[0].retweet->tweet_id == 1489944024278523906);

    REQUIRE(next_2_tweets.items[1].retweet == nullptr);
    REQUIRE(next_2_tweets.items[1].tweet != nullptr);
    REQUIRE(next_2_tweets.items[1].tweet->id == 1453461248142495744);
    REQUIRE(next_2_tweets.items[1].tweet->num_likes == 85);

    REQUIRE(next_2_tweets.get_oldest_timestamp() == 1635367140);
}


// Timeline fetching
// -----------------

TEST_CASE("Should get the beginning of a timeline") {
    set_profile_directory(TEST_PROFILE_DIR);

    Feed first_5 = Feed::get_timeline(5, -1);

    REQUIRE(first_5.items.size() == 5);

    REQUIRE(first_5.items[0].retweet != nullptr);
    REQUIRE(first_5.items[0].retweet->id == 1490135787144237058);

    REQUIRE(first_5.items[1].retweet != nullptr);
    REQUIRE(first_5.items[1].retweet->id == 1490135787124232222);

    REQUIRE(first_5.items[2].retweet != nullptr);
    REQUIRE(first_5.items[2].retweet->id == 1490119308692766723);

    REQUIRE(first_5.items[3].retweet != nullptr);
    REQUIRE(first_5.items[3].retweet->id == 1490100255987171332);

    REQUIRE(first_5.items[4].tweet != nullptr);
    REQUIRE(first_5.items[4].tweet->id == 1453461248142495744);

    REQUIRE(first_5.get_oldest_timestamp() == 1635367140);
}

TEST_CASE("Should get the middle of a timeline") {
    set_profile_directory(TEST_PROFILE_DIR);

    Feed first_5 = Feed::get_timeline(5, 1631935323);

    REQUIRE(first_5.items.size() == 5);

    REQUIRE(first_5.items[0].tweet != nullptr);
    REQUIRE(first_5.items[0].tweet->id == 1439027915404939265);

    REQUIRE(first_5.items[1].tweet != nullptr);
    REQUIRE(first_5.items[1].tweet->id == 1413773185296650241);

    REQUIRE(first_5.items[2].tweet != nullptr);
    REQUIRE(first_5.items[2].tweet->id == 1413664406995566593);

    REQUIRE(first_5.items[3].retweet != nullptr);
    REQUIRE(first_5.items[3].retweet->id == 144919526660333333);

    REQUIRE(first_5.items[4].tweet != nullptr);
    REQUIRE(first_5.items[4].tweet->id == 1413658466795737091);


    REQUIRE(first_5.get_oldest_timestamp() == 1625877417);
}
